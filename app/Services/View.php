<?php

namespace App\Services;

use Smarty;

class View
{
    public static function getSmarty()
    {
        $smarty=new smarty();

        $user = Auth::getUser();

        $smarty->settemplatedir(BASE_PATH.'/resources/views/air/');
        $smarty->setcompiledir(BASE_PATH.'/storage/framework/smarty/compile/');
        $smarty->setcachedir(BASE_PATH.'/storage/framework/smarty/cache/');
        // add config
        $smarty->assign('config', Config::getPublicConfig());
        $smarty->assign('user', Auth::getUser());
        return $smarty;
    }
}
