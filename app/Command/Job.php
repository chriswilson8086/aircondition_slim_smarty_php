<?php

namespace App\Command;



class Job
{
    public $argv;

    public function __construct($argv)
    {
        $this->argv = $argv;
    }

    public function boot()
    {
        switch ($this->argv[1]) {
            case("cron_min"):
                return cron::cron_min();
            default:
                return $this->defaultAction();
        }
    }

    public function defaultAction()
    {
        echo "Power By Chris Wilson & Liu Dos";
    }


}
