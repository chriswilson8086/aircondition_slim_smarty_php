<?php

namespace App\Utils;

use App\Models\Room;
use App\Services\Config;
use App\Models\On_service;
use App\Models\Wait_service;
use App\Models\Dr_list;
use App\Models\Dr_content;
use App\Models\Slave;


class Tools
{
    public function Warming(){
      $rooms = Room::where('status',0)->get(); //已经关机的房间
      foreach ($rooms as $room){
        if ($room->current_temp != Config::get('default_current_temp')){ //不等于温度时
          if (($room->current_temp + (Config::get('default_up_speed') * Config::get('default_time_slot')) ) < Config::get('default_current_temp')){ //假如升温
            $room->current_temp += Config::get('default_up_speed') * Config::get('default_time_slot');
          }
          else if (($room->current_temp - (Config::get('default_up_speed') * Config::get('default_time_slot')) ) < Config::get('default_current_temp')){//降温
            $room->current_temp -= Config::get('default_up_speed') * Config::get('default_time_slot');
          }
          else {
            $room->current_temp = Config::get('default_current_temp'); //最高/最低为默认温度
          }
        }
        $room->save();
      }
      $rooms = Room::where('status',3)->get(); //达到目标温度待机的房间
      foreach ($rooms as $room){
        if ($room->current_temp != Config::get('default_current_temp')){ //不等于温度时
          if (($room->current_temp + (Config::get('default_up_speed') * Config::get('default_time_slot')) ) < Config::get('default_current_temp')){ //假如升温
            $room->current_temp += Config::get('default_up_speed') * Config::get('default_time_slot');
          }
          else if (($room->current_temp - (Config::get('default_up_speed') * Config::get('default_time_slot')) ) < Config::get('default_current_temp')){//降温
            $room->current_temp -= Config::get('default_up_speed') * Config::get('default_time_slot');
          }
          else {
            $room->current_temp = Config::get('default_current_temp'); //最高/最低为默认温度
          }
        Check::checkCanServer($room->id); //检测是否需要重启
        $room->save();
      }
    }

    public function initSlave(){
      $slave = Slave::where('room_number'),Config::get('slaveID')->first();
      if ($slave == null){
        $slave = new Slave();

      }
      $slave->room_number = Config::get('slaveID');
      $slave->current_temp =  Config::get('default_current_temp');
      $slave->target_temp = Config::get('default_target_temp');
      $slave->fan_speed = Config::get('default_fan_speed');
      if ($slave->target_temp > $slave->current_temp){
        $slave->mode = 1; //升温
      }
      else {
        $slave->mode = 0; //降温
      }
      $slave->run_mode = Config::get('default_mode');
      $slave->fee = 0;
      $slave->status = 0;
      $slave->power = 0;
      $slave->wait = -1;
      $slave->save();
    }

    public function addRoom($room_number){
      $room = new Room(); //创建
      $room->room_number = $room_number;
      $room->current_temp =  Config::get('default_current_temp');
      $room->target_temp = Config::get('default_target_temp');
      $room->fan_speed = Config::get('default_fan_speed');
      if ($room->target_temp > $room->current_temp){
        $room->mode = 1; //升温
      }
      else {
        $room->mode = 0; //降温
      }
      $room->run_mode = Config::get('default_mode');
      if ($room->run_mode ==1)
        $fee_rate = explode(",",Config::get('default_save_fee_rate'));
      else
        $fee_rate = explode(",",Config::get('default_fee_rate'));
      $room->fee_rate = $fee_rate[$room->fan_speed -1];
      $room->status = 1;  //四个状态 状态0 为关机，状态1 为开机但在等待队列里， 状态2 为开机在服务队列里， 状态3 为开机但不在服务中
      $room->save();
      return $room->id;
    }

    public function setDefaultRoom($room_id){
      $room = Room::where('id',$room_id)->first();
      $room->target_temp = Config::get('default_target_temp');
      $room->fan_speed = Config::get('default_fan_speed');
      if ($room->target_temp > Config::get('default_current_temp')){
        $room->mode = 1; //升温
      }
      else {
        $room->mode = 0; //降温
      }
      $room->run_mode = Config::get('default_mode');
      if ($room->run_mode ==1)
        $fee_rate = explode(",",Config::get('default_save_fee_rate'));
      else
        $fee_rate = explode(",",Config::get('default_fee_rate'));
      $room->fee_rate = $fee_rate[$room->fan_speed -1];
      $room->status = 1;  //为开机但在等待队列里
      $room->save();
      return $room->id;
    }

    public function setRoomTemp($current_temp,$room_id){
      $room = Room::where('id',$room_id)->first();
      $room->current_temp = $current_temp;
      $room->save();
    }

    public function updateTodo($room_id){
      $room = Room::where('id',$room_id)->first();
      $service = On_service::where('room_id',$room_id)->first();
      $content = json_decode($service->todo);
      $fan_speed = $content['fan_speed'];
      $run_mode = $content['run_mode'];
      $current_temp = $content['current_temp'];
      $target_temp = $content['target_temp'];
      if ($fan_speed == 0)
        $fan_speed = Config::get('default_fan_speed');
      if ($run_mode == 0)
        $run_mode = Config::get('default_mode');
        $room->target_temp = $target_temp;
        $room->current_temp = $current_temp;
        $room->fan_speed = $fan_speed;
        $room->run_mode = $run_mode;
        if ($room->target_temp > Config::get('default_current_temp')){
          $room->mode = 1; //升温
        }
        else {
          $room->mode = 0; //降温
        }
        $room->run_mode = Config::get('default_mode');
        if ($room->run_mode ==1)
          $fee_rate = explode(",",Config::get('default_save_fee_rate'));
        else
          $fee_rate = explode(",",Config::get('default_fee_rate'));
        $room->fee_rate = $fee_rate[$room->fan_speed -1];
        $room->save();
        Tools::endDr($room_id); //结束账单
      }

    public function addRoomToWait($room_id){
      $room = Room::where('id',$room_id)->first();
      $service = new Wait_service();
      $service->room_id = $room_id;
      $service->service_time = 0;
      $service->status = $room->status;
      $service->save();
    }

    public function newDr($room_id){
      $room = Room::where('id',$room_id)->first();
      //处理详单，假如有未结账的详单列表，则处理该详单列表，否则新建详单列表
      $dr_list = Dr_list::where('room_id',$room_id)->where('is_check_out',0)->first();
      if ($dr_list == null){
        $dr_list = new Dr_list();
        $dr_list->room_id = $room_id;
        $dr_list->date = time();
        $dr_list->save();
      }
      $dr_content = new Dr_content();
      $dr_content->room_id = $room_id;
      $dr_content->list_id = $dr_list->id;
      $dr_content->service_time = 0;
      $dr_content->start_time = time();
      $dr_content->target_temp = $room->target_temp;
      $dr_content->fan_speed = $room->fan_speed;
      $dr_content->run_mode = $room->run_mode;
      $dr_content->fee_rate = $rooms->fee_rate;
      $dr_content->fee = 0;
      if ($room->run_mode ==1)
        $power = explode(",",Config::get('default_save_power_rate'));
      else
        $power = explode(",",Config::get('default_power_rate'));
      $dr_content->power = $power[$room->fan_speed -1];
      $dr_content->save();
    }

    public function endDr($room_id){
      $service = Wait_service::where('room_id',$room_id)->first();
      if ($service == null){
        $service = On_service::where('room_id',$room_id)->first();
      }
      if ($service != null){
        $dr_content->service_time = $service->service_time;
      }
      $dr_content = Dr_content::where('room_id',$room_id)->orderBy('id','desc')->first();
      $dr_content->end_time= time();
      $dr_content->fee = round(($dr_content->service_time/60) * $dr_content->fee_rate * 100) / 100;
      $dr_content->save();
    }

    public function getWait($room_id){
      $room = Room::where('id',$room_id)->first();
      if ($room->status == 0)
        return -1; //关机状态
      else if ($room->status == 2)
        return 0; //正在服务
      else if ($room->status == 3)
        return -2; //达到目标温度后自动停机
      else {
        if (Config::get('is_priority')){ //假如运行在优先级模式下
          $count = Room::where('status',1)->('fan_speed','>',$room->fan_speed)->count(); //大于该从机的统计
          $rooms = Room::where('status',1)->('fan_speed','=',$room->fan_speed)->get(); //等于的处理
          $nowservice = Wait_service::where('room_id',$room_id)->first();
          foreach ($rooms as $room){
            $service = Wait_service::where('room_id',$room->id)->first();
            if ($service->id < $nowservice->id)
              $count ++;
          }
          return $count;
        }
        else {
          $service = Wait_service::where('room_id',$room_id)->first();
          return On_service::count() + Wait_service::where('id','<',$service->id)->count();
        }
      }
    }

    public function getPower($room_id){
      $dr_list = Dr_list::where('room_id',$room_id)->where('is_check_out',0)->first();
      if ($dr_list != null){
        $dr_contents = Dr_content::where('list_id',$dr_list->id)->get();
        $allpower = 0;
        $count = Dr_content::where('list_id',$dr_list->id)->count();
        foreach ($dr_contents as $dr_content){
          $allpower += $dr_content->power;
        }
        return round($allpower / $count);
      }
      return 0;
    }

    public function getFee($room_id){
      $room = Room::where('id',$room_id)->first();
      $dr_list = Dr_list::where('room_id',$room_id)->where('is_check_out',0)->first();
      if ($dr_list != null){
        $dr_contents = Dr_content::where('list_id',$dr_list->id)->get();
        $fee = 0;
        foreach ($dr_contents as $dr_content){
          $fee += $dr_content->fee;
        }
        $service = Wait_service::where('room_id',$room_id)->first();
        if ($service == null){
          $service = On_service::where('room_id',$room_id)->first();
        }
        if ($service != null){
          $fee += round(($service->service_time /60) * $room->fee_rate * 100) / 100;
        }
        return $fee;
      }
      return 0;
    }
}
