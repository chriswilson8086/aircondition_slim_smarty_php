<?php

namespace App\Utils;

use App\Models\Room;
use App\Services\Config;
use App\Models\On_service;
use App\Models\Wait_service;
use App\Models\Dr_list;
use App\Models\Dr_content;


class Check
{
    public function checkroom($room_number,$func){ //用于处理Room类
      if ($func == 0){ //开机操作（假如存在则赋值默认，不存在则创建）
        if(Room::where('room_number',$room_number)->count() == 0){ //不存在该房间记录
          return Tools::addRoom($room_number); //创建
        }
        else { //存在
          $room = Room::where('room_number',$room_number)->first();
          if ($room->status !=0){ //不为关机状态，则返回失败
            return 0;
          }
          else { //状态正确
            if (Dr_list::where('room_id',$room_id)->where('is_check_out',0)->count() ==0 ){ //为退房后的新房客
              return Tools::setDefaultRoom($room->id); //设置为默认值
            }
            else {//为退房再次开机
              $room->status = 1;
              $room->save();
              return $room->id; //一切从数据库里读取
            }
          }
        }
      }
      else{ //关机操作或修改温度操作或获取当前信息操作
        if(Room::where('room_number',$room_number)->count() == 0){ //不存在该房间记录
          $room_id = Tools::addRoom($room_number); //创建
          if ($func == 1)
            return 0; //返回失败
          else
            return $room_id; //定时请求返回room_id
        }
        else if ($func == 1){
          $room = Room::where('room_number',$room_number)->first();
          if ($room->status == 0) //如果为关机状态
            return 0;
          else
            return $room->id;
        }
        else { //如果是定时请求
          $room = Room::where('room_number',$room_number)->first();
          return $room->id;
        }
      }
    }

    public function checkCanServer($room_id){ //判断是否温度需要服务
      $room = Room::where('id',$room_id)->first();
      if (abs($room->current_temp - $room->target_temp) >= Config::get('default_temp_slot')){
        $room->status = 1; //超过阈值温度后重启
        $room->save();
        Tools::addRoomToWait($room_id); //添加到等待队列
        Tools::newDr($room_id); //新建账单
      }
    }

    public function setStatusOff($room_id){ //不在此处出队列，按时间片走
      $room = Room::where('id',$room_id)->first();
      $room->status = 0;
      $room->save();
      $service = Wait_service::where('room_id',$room_id)->first();
      if ($service == null){
        $service = On_service::where('room_id',$room_id)->first();
      }
      if ($service != null){
        $service->status = 0;
        $service->save();
      }
    }

    public function editConf($room_id, $target_temp, $current_temp, $fan_speed, $run_mode){
      $room = Room::where('id',$room_id)->first();
      if ($fan_speed == 0)
        $fan_speed = Config::get('default_fan_speed');
      if ($run_mode == 0)
        $run_mode = Config::get('default_mode');
      if ($room->status != 2){
        $room->target_temp = $target_temp;
        $room->current_temp = $current_temp;
        $room->fan_speed = $fan_speed;
        $room->run_mode = $run_mode;
        if ($room->target_temp > Config::get('default_current_temp')){
          $room->mode = 1; //升温
        }
        else {
          $room->mode = 0; //降温
        }
        if ($room->run_mode ==1)
          $fee_rate = explode(",",Config::get('default_save_fee_rate'));
        else
          $fee_rate = explode(",",Config::get('default_fee_rate'));
        $room->fee_rate = $fee_rate[$room->fan_speed -1];
        $room->save();
        if ($room->status == 1){
          Tools::endDr($room_id); //结束账单
          Tools::newDr($room_id); //如果在等待队列中，新建账单
        }
        else {
          Check::checkCanServer($room_id); //如果在待机状态，则检测是否需要开机,开机则创建新账单
        }
      }
      else {
        $service = On_service::where('room_id',$room_id)->first();
        if ($service !=null){ //假如在服务队列中
          $todo = array(
            "target_temp" => $target_temp,
            "current_temp" => $current_temp,
            "fan_speed" => $fan_speed,
            "run_mode" => $run_mode,
          );
          $service->todo = json_encode($todo);
          $service->save();
        }
        //正在运行的不结束账单，等待
      }
    }

    public function checkEditLegal($target_temp, $fan_speed, $run_mode){
        if ($target_temp > Config::get('default_max_target_temp') || $target_temp < Config::get('default_min_target_temp'))
          return 0;
        if (!is_int($fan_speed) || $fan_speed >3 || $fan_speed <0)
          return 0;
        if (!is_int($run_mode) || $run_mode >3 || $run_mode <0)
          return 0;
        return 1;
    }


    public function checkService(){
      Schedule::scheduleOn();
      if (Config::get('is_priority'))
        Schedule::scheduleWait_withPriority();
      else
        Schedule::scheduleWait();
    }

}
