<?php

namespace App\Utils;

use App\Models\On_service;
use App\Models\Wait_service;
use App\Models\Dr_list;
use App\Models\Dr_content;
use App\Models\Room;
use App\Services\Config;


class Schedule
{
    public function scheduleOn(){
      $allservice = On_service::all();
      foreach ($allservice as $service){
        $room = Room::where('id',$service->room_id)->first();
        $service->ttl --;
        $service->service_time += Config::get('default_time_slot');
        //降温升温操作
        if ($room->run_mode == 1)
          $desc_speed = explode(",",Config::get('default_save_desc_speed'));
        else
          $desc_speed = explode(",",Config::get('default_desc_speed'));
        if ($room->mode == 0)
          $room->current_temp -= ($desc_speed * (Config::get('default_time_slot') /60 ));
        else
          $room->current_temp += ($desc_speed * (Config::get('default_time_slot') /60 ));
        if ($service->status ==0){ //关机请求
          $room->save();
          Tools::endDr($room->id); //关闭账单
          $service->delete();
          continue;
        }
        if ($service->todo != null){ //在服务队列中有设置变更请求
          Tools::updateTodo($room->id); //更新设置并关闭账单
          if (abs($room->current_temp - $room->target_temp) > 0.1){ //更新设置后假如室温与目标温度不同
            $room->status = 1; //进入就绪队列
            Tools::addRoomToWait($room->id); //加入就绪队列
            Tools::newDr($room->id); //开新详单
            $room->save();
            $service->delete();
            continue;
          }
        }
        if (abs($room->current_temp - $room->target_temp) < 0.1){ //达到目标温度
          $room->status = 3; //休眠
          $room->save();
          Tools::endDr($room->id); //关闭账单
          $service->delete();
          continue;
        }

        if ($service->ttl == 0){ //执行完ttl
          $room->status = 1; //加入等待队列
          $room->save();
          //新建等待队列
          $wait = new Wait_service();
          $wait->room_id = $room->id;
          $wait->service_time = $service->service_time;
          $wait->status = 1;
          $wait->save();
          $service->delete();
          continue;
        }
        $room->save();
        $service->save();
      }
    }

    public function scheduleWait(){
      $allservice = Wait_service::all();
      $free_slot = Config::get('default_service_slot') - On_service::count();
      foreach ($allservice as $service){
        $room = Room::where('id',$service->room_id)->first();
        if ($service->status == 0){
          $room->save();
          Tools::endDr($room->id); //关闭账单
          $service->delete();
          continue;
        }
        if($free_slot !=0){
          $on = new On_service();
          $on->room_id = $room->id;
          $on->service_time = $service->service_time;
          $on->ttl = Config::get('default_ttl');
          $on->status = 2;
          $room->status = 2;
          $on->save();
          $room->save();
          $service->delete();
          $free_slot --;
        }
      }
    }

    public function scheduleWait_withPriority(){
      $rooms = Room::where('status',1)->orderBy('fan_speed','desc')->get(); //将等待状态的房间按风速由高到低排序
      $free_slot = Config::get('default_service_slot') - On_service::count();
      foreach ($rooms as $room){
        $service = Wait_service::where('room_id',$room->id)->first();
        if ($service->status == 0){
          $room->save();
          Tools::endDr($room->id); //关闭账单
          $service->delete();
          continue;
        }
        if($free_slot !=0){
          $on = new On_service();
          $on->room_id = $room->id;
          $on->service_time = $service->service_time;
          $on->ttl = Config::get('default_ttl');
          $on->status = 2;
          $room->status = 2;
          $on->save();
          $room->save();
          $service->delete();
          $free_slot --;
        }
      }
    }
}
