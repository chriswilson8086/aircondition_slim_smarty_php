<?php

namespace App\Controllers;

use App\Models\Room;
use App\Services\Config;
use App\Models\On_service;
use App\Models\Wait_service;
use App\Models\Dr_list;
use App\Models\Dr_content;
use App\Models\Slave;
use App\Utils\Check;
use App\Utils\Tools;
use Ozdemir\Datatables\Datatables;
use App\Utils\DatatablesHelper;
/**
 *  HomeController
 */
class HomeController extends BaseController
{
    public function index($request, $response, $args)
    {
		  return $this->view()->display('index.tpl');
    }
    public function slave_index($request, $response, $args)
    {
      $slave = Slave::where('room_number'),Config::get('slaveID')->first();
      if ($slave->wait == -1)
        $status = '关机';
      else if ($slave->wait == -2)
        $status = '待机';
      else if ($slave->wait == 0)
        $stauts = '正在服务';
      else
        $status = '等待服务，前方有：'.$slave->wait.'台空调在排队';
      switch ($slave->fan_speed){
        case 1:
          $fan_speed = '低风速';
          break;
        case 2:
          $fan_speed = '中风速';
          break;
        case 3:
          $fan_speed = '高风速';
          break;
        default:
          $fan_speed = '-1S';
          break;
      }
      return $this->view()->assign('slave', $slave)->assign('status', $status)->assign('fan_speed', $fan_speed)->assign('max_temp', Config::get('default_max_target_temp'))->assign('min_temp', Config::get('default_min_target_temp'))->display('slaveindex.tpl');
    }

    public function requestOn($request, $response, $args)
    {
        $slave = Slave::where('room_number'),Config::get('slaveID')->first();
        if ($slave->status !=0){
          $rs['ret'] = 0;
          $rs['msg'] = "您已经处于开机状态，请勿重复开机。";
          return $response->getBody()->write(json_encode($rs));
        }
        $slave->status = 1;
        $slave->save();
        $status = ApiController::post_slave_on(Config::get('slaveID'));
        if ($status){
          $rs['ret'] = 1;
          $rs['msg'] = "开机成功";
        }
        else {
          $rs['ret'] = 0;
          $rs['msg'] = "开机失败";
        }
        return $response->getBody()->write(json_encode($rs));
    }

    public function requestOff($request, $response, $args)
    {
        $slave = Slave::where('room_number'),Config::get('slaveID')->first();
        if ($slave->status ==0){
          $rs['ret'] = 0;
          $rs['msg'] = "您已经处于关机状态，请勿重复关机。";
          return $response->getBody()->write(json_encode($rs));
        }
        $slave->status = 0;
        $slave->save();
        $status = ApiController::post_slave_off(Config::get('slaveID'));
        if ($status){
          $rs['ret'] = 1;
          $rs['msg'] = "关机成功";
        }
        else {
          $rs['ret'] = 0;
          $rs['msg'] = "关机失败";
        }
        return $response->getBody()->write(json_encode($rs));
    }

    public function page404($request, $response, $args)
    {
        $newResponse = $response->withStatus(404);
        $newResponse->getBody()->write($this->view()->display('404.tpl'));
        return $newResponse;
    }

    public function page405($request, $response, $args)
    {
        $newResponse = $response->withStatus(405);
        $newResponse->getBody()->write($this->view()->display('405.tpl'));
        return $newResponse;
    }

    public function page500($request, $response, $args)
    {
        $newResponse = $response->withStatus(500);
        $newResponse->getBody()->write($this->view()->display('500.tpl'));
    }

    public function getnew($request, $response, $args)
    {
        $this->user = Auth::getUser();
        $id = $request->getParam('id');
        $band = Band::where('id','=',$id)->first();
        if ($band->user_id == $this->user->id){
        	$band->is_sleep = 2;
            $band->save();
            $rs['ret'] = 1;
        	$rs['msg'] = "正在获取，请等待页面自动刷新。";
        }
        else {
        	$rs['ret'] = 0;
        	$rs['msg'] = "您没有权限获取改手环当前位置。";
        }

        return $response->getBody()->write(json_encode($rs));
    }

    public function getband($request, $response, $args)
    {
        $bands = Band::all();
        $allband = array();
        foreach ($bands as $band){
        	array_push($allband,array('token'=>$band->token,'value'=>$band->is_sleep));
        }
        return $response->getBody()->write(json_encode($allband));
    }

    public function postlocate($request, $response, $args)
    {
        $token =  $request->getParam('token');
        $lng =  $request->getParam('lng');
        $lat =  $request->getParam('lat');
        $band = Band::where('token','=',$token)->first();
        if ($band->is_sleep == 2){
        	$band->is_sleep = 0;
            $band->save();
        }
        if ($band ==NULL){
   		    $rs['ret'] = 0;
    	    $rs['msg'] = "Error Token!";
        }
        else {
        	$locate = new Locate();
            $locate->lng = $lng;
            $locate->lat = $lat;
            $locate->band_id = $band->id;
            $locate->time = time();
            $locate->save();
          	$rs['ret'] = 1;
            $rs['msg'] = "Sucess!";
        }
        return $response->getBody()->write(json_encode($rs));
    }

    public function bandlist($request, $response, $args)
    {
        $table_config['total_column'] = array("op" => "操作", "id" => "ID",
                              "name" => "使用人", "token" => "手环Token", "is_sleep"=>"手环状态");
        $table_config['default_show_column'] = array("op", "id",
                                                    "name", "token", "is_sleep");
        $table_config['ajax_url'] = '/user/band/ajax';
        return $this->view()->assign('table_config', $table_config)->display('bandlist.tpl');
    }

    public function addband_get($request, $response, $args)
    {
        return $this->view()->display('addband.tpl');
    }


    public function addband_post($request, $response, $args)
    {
        $token =  $request->getParam('token');
        $name =  $request->getParam('name');
        $start_hour = $request->getParam('start_hour');
        $start_min = $request->getParam('start_min');
     	$end_hour = $request->getParam('end_hour');
     	$end_min = $request->getParam('end_min');
        if ($token==null|| $name==null || $start_hour==null || $start_min==null || $end_hour==null || $end_min==null){
        	$rs['ret'] = 0;
            $rs['msg'] = "请填全！";
            return $response->getBody()->write(json_encode($rs));
        }
        $band = Band::where('token','=',$token)->count();
        if ($band !=0){
        	$rs['ret'] = 0;
            $rs['msg'] = "该Token已使用";
            return $response->getBody()->write(json_encode($rs));
        }
        $this->user = Auth::getUser();
        $band = new Band();
        $band->token = $token;
        $band->name = $name;
        $band->user_id = $this->user->id;
        $band->is_sleep = 0;
        $band->save();
        $temp = new Configs();
        $temp->band_id = $band->id;

        $temp->start_hour= $start_hour;
        $temp->start_min= $start_min;
        $temp->end_hour= $end_hour;
        $temp->end_min= $end_min;
        $temp->start= ($temp->start_hour * 3600) + ($temp->start_min * 60);
        $temp->end = ($temp->end_hour * 3600) + ($temp->end_min * 60);
        if (!$temp->is_legal()){
             $band->delete();
          	 $rs['ret'] = 0;
          	 $rs['msg'] = "时间设置错误";

          }
          else{
  	      	$temp->save();
          	$rs['ret'] = 1;
          	$rs['msg'] = "添加成功";
          }
        $rs['ret'] = 1;
        $rs['msg'] = "添加成功!";
        return $response->getBody()->write(json_encode($rs));
    }

    public function band_edit($request, $response, $args)
    {
        $id = $args['id'];
        $this->user = Auth::getUser();
        $band = Band::where('id','=',$id)->where('user_id','=',$this->user->id)->first();
        if ($band == null) {
        }
        $configs = Configs::where('band_id','=',$id)->first();
        return $this->view()->assign('band', $band)->assign('configs', $configs)->display('bandedit.tpl');
    }

    public function band_delete($request, $response, $args)
    {
        $id =  $request->getParam('id');
        $this->user = Auth::getUser();
        $band = Band::where('id','=',$id)->where('user_id','=',$this->user->id)->first();
        if ($band == null) {
          $rs['ret'] = 0;
          $rs['msg'] = "手环不存在或您无权删除";
        }
        else {
          Configs::where('band_id','=',$id)->delete();
          Locate::where('band_id','=',$id)->delete();
          $band->delete();
          $rs['ret'] = 1;
          $rs['msg'] = "删除成功";
        }
        return $response->getBody()->write(json_encode($rs));
    }

    public function band_update($request, $response, $args)
    {
        $id = $args['id'];
        $token =  $request->getParam('token');
        $name =  $request->getParam('name');
        $start_hour = $request->getParam('start_hour');
        $start_min = $request->getParam('start_min');
     	$end_hour = $request->getParam('end_hour');
     	$end_min = $request->getParam('end_min');
        if ($token==null|| $name==null || $start_hour==null || $start_min==null || $end_hour==null || $end_min==null){
        	$rs['ret'] = 0;
            $rs['msg'] = "请填全！";
            return $response->getBody()->write(json_encode($rs));
        }
        $this->user = Auth::getUser();
        $band = Band::where('id','=',$id)->first();
        if ($band->user_id == $this->user->id){
   	     $band->token = $token;
    	   $band->name = $name;
  	      $band->user_id = $this->user->id;
   	      $band->is_sleep = 0;
   	      $temp = Configs::where('band_id','=',$id)->first();
   	      $temp->band_id = $band->id;
   	      $temp->start_hour= $start_hour;
   	      $temp->start_min= $start_min;
   	      $temp->end_hour= $end_hour;
  	      $temp->end_min= $end_min;
          $temp->start= ($temp->start_hour * 3600) + ($temp->start_min * 60);
          $temp->end = ($temp->end_hour * 3600) + ($temp->end_min * 60);
          if (!$temp->is_legal()){
          	 $rs['ret'] = 0;
          	 $rs['msg'] = "时间设置错误";

          }
          else{
          	$band->save();
  	      	$temp->save();
          	$rs['ret'] = 1;
          	$rs['msg'] = "修改成功";
          }
        }
        else {
          $rs['ret'] = 0;
          $rs['msg'] = "您无权修改";
        }
        return $response->getBody()->write(json_encode($rs));
    }

    public function band_ajax($request, $response, $args)
    {
        $this->user = Auth::getUser();
        $datatables = new Datatables(new DatatablesHelper());
        $datatables->query('Select id as op,id,name,token,is_sleep from band where user_id = '.$this->user->id);

        $datatables->edit('op', function ($data) {
            return '<a class="btn btn-brand" href="/user/band/'.$data['id'].'/edit">修改</a>
                    <a class="btn btn-brand-accent" id="delete" value="'.$data['id'].'" href="javascript:void(0);" onClick="delete_modal_show(\''.$data['id'].'\')">删除</a>';
        });

        $datatables->edit('is_sleep', function ($data) {
            if($data['is_sleep']!=1)
              return "正常";
            else
              return "休眠";
        });

        $datatables->edit('DT_RowId', function ($data) {
            return 'row_1_'.$data['id'];
        });

        $body = $response->getBody();
        $body->write($datatables->generate());
    }



}
