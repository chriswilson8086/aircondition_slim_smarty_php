<?php

namespace App\Controllers;

use App\Services\Config;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

use App\Utils\Check;
use App\Utils\Schedule;
use App\Models\Room;
use App\Models\Slave;
use App\Utils\Tools;

class ApiController extends BaseController
{
    public function requestOn($current_temp, $room_number, $response){
      $room_id = Check::checkroom($room_number, 0); //判断是否有该room_number的记录，没有的话新建该room到数据库，返回room_id,有的话赋值初始值
      if ($room_id ==0){
        $status = false;
      }
      else {
        Tools::setRoomTemp($current_temp, $room_id); //更新当前温度
        Tools::addRoomToWait($room_id); //添加到等待队列
        Tools::newDr($room_id);
        $status = true;
      }
      $data = array(
              "sucess" => $status,
              "msg" => '',
              "type" => 1,
               );
      return $response->getBody()->write(json_encode($data));
    }

    public function requestOff($room_number, $response){
      $room_id = Check::checkroom($room_number, 1); //判断是否有该room_number的记录，没有的话新建该room到数据库，返回room_id,有的话赋值初始值
      if ($room_id ==0){
        $status = false;
      }
      else {
        Check::setStatusOff($room_id);
        $status = true;
      }
      $data = array(
              "sucess" => $status,
              "msg" => '',
              "type" => 2,
               );
      return $response->getBody()->write(json_encode($data));
    }

    public function requestEdit($room_number, $target_temp, $current_temp, $fan_speed, $run_mode, $response){
      $room_id = Check::checkroom($room_number, 1); //判断是否有该room_number的记录，没有的话新建该room到数据库，返回room_id
      if ($room_id ==0){
        $status = false;
      }
      else {
        if (Check::checkEditLegal()){
          Check::editConf($room_number, $target_temp, $current_temp, $fan_speed, $run_mode);
          $status = true;
        }
        else {
          $status = false;
        }
      }
      $msg = array(
        "state" => $status,
        "waited" => Tools::getWait($room_id),
      )
      $data = array(
              "sucess" => $status,
              "msg" => $msg,
              "type" => 3,
               );
      return $response->getBody()->write(json_encode($data));
    }

    public function requestInfo($room_number, $response){
      $room_id = Check::checkroom($room_number, 2); //判断是否有该room_number的记录，没有的话新建该room到数据库，返回room_id,有的话赋值初始值
      if ($room_id ==0){
        $status = false;
      }
      $room = Room::where('id',$room_id)->first();
      $msg = array(
        "temperature" => $room->current_temp,
        "power" => Tools::getPower($room_id),
        "consumption" => Tools::getFee($room_id),
        "waited" => Tools::getWait($room_id),
      )
      $data = array(
              "sucess" => $status,
              "msg" => $msg,
              "type" => 4,
               );
      return $response->getBody()->write(json_encode($data));
    }

    public function post_slave_on($slaveID){
      $slave = Slave::where('room_number'),$slaveID->first();
      if ($slave == null){
        $slave = Room::where('room_number',$slaveID)->first();
        if ($slave == null){
          return false;
        }
      }
      $ch = curl_init();
      $headers = array('Content-Type: application/json');
      $data = array(
        "initial_temperature" => $slave->current_temp,
        "room_number" => $slaveID,
        "type" => 1,
                  );
      $json = json_encode($data);
      curl_setopt($ch, CURLOPT_URL, Config::get('masterUrl'));
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $result = curl_exec($ch);
      curl_close($ch);
      $result = json_decode($result,TRUE);
      if ($result["sucess"] == true)
        return true;
      else
        return false;
    }

    public function post_slave_off($slaveID){
      $slave = Slave::where('room_number'),$slaveID->first();
      if ($slave == null){
        $slave = Room::where('room_number',$slaveID)->first();
        if ($slave == null){
          return false;
        }
      }
      $ch = curl_init();
      $headers = array('Content-Type: application/json');
      $data = array(
        "room_number" => $slaveID,
        "type" => 2,
                  );
      $json = json_encode($data);
      curl_setopt($ch, CURLOPT_URL, Config::get('masterUrl'));
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $result = curl_exec($ch);
      curl_close($ch);
      $result = json_decode($result,TRUE);
      if ($result["sucess"] == true)
        return true;
      else
        return false;
    }

    public function post_slave_info($slaveID){
      $slave = Slave::where('room_number'),$slaveID->first();
      if ($slave == null){
        $slave = Room::where('room_number',$slaveID)->first();
        if ($slave == null){
          return false;
        }
      }
      $ch = curl_init();
      $headers = array('Content-Type: application/json');
      $data = array(
        "room_number" => $slaveID,
        "type" => 4,
                  );
      $json = json_encode($data);
      curl_setopt($ch, CURLOPT_URL, Config::get('masterUrl'));
      curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $result = curl_exec($ch);
      curl_close($ch);
      $result = json_decode($result,TRUE);
      $slave = Slave::where('room_number'),$slaveID->first(); //处理的是Slave数据
      if ($slave != null){
        $slave->current_temp = $result['msg']['temperature'];
        $slave->power = $result['msg']['power'];
        $slave->fee = $result['msg']['consumption'];
        $slave->wait = $result['msg']['waited'];
        if ($slave->wait == -1)
          $slave->status = 0;
        else if ($slave->wait == -2)
          $slave->status = 3;
        else if ($slave->wait == 0)
          $slave->status = 2;
        else
          $slave->status = 1;
        $slave->save();
      }
      else {
        $room = Room::where('room_number',$slaveID)->first();
        if ($room == null){
          return false;
        }
        $room->current_temp = $result['msg']['temperature'];
        $room->save();
      }

    }

    public function get_request($request, $response, $args)
    {
      $type = $request->getParam('type');
      switch ($type){
        case 1:
          ApiController::requestOn($request->getParam('initial_temperature'), $request->getParam('room_number'), $response);
          break;
        case 2:
          ApiController::requestOff($request->getParam('room_number'), $response);
          break;
        case 3:
          ApiController::requestEdit($request->getParam('room_number'), $request->getParam('target_temperature'), $request->getParam('current_temperature'), $request->getParam('wind_speed'), $request->getParam('mode'), $response);
          break;
        case 4:
          ApiController::requestInfo($request->getParam('room_number'), $response);
          break;
        default:
        $data = array(
                "sucess" => false,
                "msg" => 'I am angry! U R too young too sample!',
                "type" => 65535,
                 );
        return $response->getBody()->write(json_encode($data));
        break;
      }
    }

}
