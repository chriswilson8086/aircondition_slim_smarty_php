<?php


use App\Services\Boot;

//  BASE_PATH
define('BASE_PATH', __DIR__);

// Vendor Autoload
require BASE_PATH.'/vendor/autoload.php';
// Config Load
require BASE_PATH."/config/.config.php";



Boot::loadEnv();
Boot::setDebug();

Boot::setTimezone();
Boot::bootDb();
