<?php

#基础信息设置
$System_Config['key'] = 'asdfhcvhbdtayhiaert894u394tufdgdf'; //随机输就行
$System_Config['debug'] =  'true'; //放生产环境请设置为false
$System_Config['appName'] = '空调管理系统';  // 站点名称
$System_Config['baseUrl'] = 'http://air.com';            // 站点地址
$System_Config['timeZone'] = 'PRC';
$System_Config['authDriver'] = 'cookie';
$System_Config['sessionDriver'] = 'cookie';
$System_Config['cacheDriver'] = 'cookie';
$System_Config['tokenDriver'] = 'db';
$System_Config['masterUrl'] = 'http://air.com'; //主机地址
$System_Config['slaveID'] = '2003'; //房间ID
$System_Config['is_priority'] = '0'; //是否运行于优先级模式下


# database 数据库配置
$System_Config['db_driver'] = 'mysql';
$System_Config['db_host'] = 'localhost';
$System_Config['db_database'] = 'air';
$System_Config['db_username'] = 'air';
$System_Config['db_password'] = 'Root1234';
$System_Config['db_charset'] = 'utf8';
$System_Config['db_collation'] = 'utf8_general_ci';
$System_Config['db_prefix'] = '';

# 空调默认值设置
$System_Config['default_service_slot'] = '4'; //默认同时服务数量
$System_Config['default_time_slot'] = '10'; //默认时间片(秒)
$System_Config['default_mode'] = '2'; //默认运行模式,1节能,2高效
$System_Config['default_current_temp'] = '26'; //从机默认室温
$System_Config['default_target_temp'] = '22'; //从机默认温度
$System_Config['default_fan_speed'] = '2'; //1为低速，2为中速，3为高速
$System_Config['default_fee_rate'] = '0.2,0.5,1'; //分别对应三档风速每分钟价格 高效
$System_Config['default_power_rate'] = '150,400,800'; //分别对应三档风速功率 高效
$System_Config['default_desc_speed'] = '0.3,0.5,1'; //分别对应三档风速降温速度分钟 高效
$System_Config['default_save_fee_rate'] = '0.1,0.25,0.5'; //分别对应三档风速每分钟价格 节能
$System_Config['default_save_power_rate'] = '100,250,500'; //分别对应三档风速功率 节能
$System_Config['default_save_desc_speed'] = '0.2,0.4,0.8'; //分别对应三档风速降温速度分钟 节能
$System_Config['default_up_speed'] = '0.1'; //房间升温速度分钟
$System_Config['default_temp_slot'] = '0.1'; //降/升温到目标温度后重启阈值
$System_Config['default_max_target_temp'] = '32'; //最大目标温度
$System_Config['default_min_target_temp'] = '16'; //最小目标温度
$System_Config['default_ttl'] = '2'; //ttl
