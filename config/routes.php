<?php

use Slim\App;
use Slim\Container;
use App\Controllers;
use Zeuxisoo\Whoops\Provider\Slim\WhoopsMiddleware;

/***
 * The slim documents: http://www.slimframework.com/docs/objects/router.html
 */

// config
$debug = false;
if (defined("DEBUG")) {
    $debug = true;
}


$configuration = [
    'settings' => [
        'debug' => $debug,
        'whoops.editor' => 'sublime',
        'displayErrorDetails' => $debug
    ]
];

$container = new Container($configuration);

$container['notFoundHandler'] = function ($c) {
    return function ($request, $response) use ($c) {
        return $response->withAddedHeader('Location', '/404');
    };
};

$container['notAllowedHandler'] = function ($c) {
    return function ($request, $response, $methods) use ($c) {
        return $response->withAddedHeader('Location', '/405');
    };
};

if ($debug==false) {
    $container['errorHandler'] = function ($c) {
        return function ($request, $response, $exception) use ($c) {
            return $response->withAddedHeader('Location', '/500');
        };
    };
}

$app = new App($container);
$app->add(new WhoopsMiddleware);


// Home
$app->get('/', 'App\Controllers\HomeController:index');
$app->get('/front', 'App\Controllers\HomeController:front_index'); //前台首页
$app->get('/airmanager', 'App\Controllers\HomeController:airmanager_index'); //空调管理员首页
$app->get('/manager', 'App\Controllers\HomeController:manager_index'); //酒店经理首页
// slave
$app->get('/slave', 'App\Controllers\HomeController:slave_index'); //客户首页
$app->post('/slave/on', 'App\Controllers\HomeController:slave_on'); //客户开机
$app->post('/slave/off', 'App\Controllers\HomeController:slave_off'); //客户关机
$app->post('/slave/edit', 'App\Controllers\HomeController:slave_edit'); //客户修改设置
// FakeApi

$app->post('/', 'App\Controllers\ApiController:get_request');

// Error Page
$app->get('/404', 'App\Controllers\HomeController:page404');
$app->get('/405', 'App\Controllers\HomeController:page405');
$app->get('/500', 'App\Controllers\HomeController:page500');


$app->run();
