{include file='userheader.tpl'}







	<main class="content">
		<div class="content-header ui-content-header">
			<div class="container">
				<h1 class="content-heading">从机面板---{$slave->room_number}</h1>
			</div>
		</div>
		<div class="container">
			<section class="content-inner margin-top-no">
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="card margin-bottom-no">
							<div class="card-main">
								<div class="card-inner">
									<div class="card-inner">
										<p class="card-heading">空调状态</p>
										<h6>运行状态：</h6>
										<p>{$status}</p><br/>
										<h6>当前费用：</h6>
										<p>{$slave->fee}</p><br/>
										<h6>当前室温：</h6>
										<p>{$slave->current_temp}°C</p><br/>
										<h6>当前目标温度：</h6>
										<p>{$slave->target_temp}°C</p><br/>
										<h6>当前模式：</h6>
										<p>{$slave->mode ? "升温" : "降温"}</p><br/>
										<h6>当前运行模式：</h6>
										<p>{($slave->run_mode == 1) ? "节能" : "高效"}</p><br/>
										<h6>当前风速：</h6>
										<p>{$fan_speed}</p><br/>
										<h6>当前平均功率：</h6>
										<p>{$slave->power}W</p><br/>
									</div>

								</div>
							</div>
						</div>
					</div>

					<div class="col-lg-12 col-md-12">
						<div class="card margin-bottom-no">
							<div class="card-main">
								<div class="card-inner">
									<div class="card-inner">
										<p class="card-heading">空调控制</p>

										<div class="form-group form-group-label">
											<label class="floating-label" for="target_temp">目标温度</label>
											<input class="form-control" id="target_temp" type="text" value="{round($slave->target_temp)}">
										</div>

										<div class="form-group form-group-label">
										<label for="fan_speed">
											<label class="floating-label" for="fan_speed">风速</label>
												<select id="fan_speed" class="form-control" name="fan_speed">
													<option value="1" {if $slave->fan_speed == 1}selected{/if}>低风速</option>
													<option value="2" {if $slave->fan_speed == 2}selected{/if}>中风速</option>
													<option value="3" {if $slave->fan_speed == 3}selected{/if}>高风速</option>
												</select>
										  </label>
									  </div>

										<div class="form-group form-group-label">
										<label for="run_mode">
											<label class="floating-label" for="run_mode">风速</label>
												<select id="run_mode" class="form-control" name="run_mode">
													<option value="1" {if $slave->run_mode == 1}selected{/if}>节能</option>
													<option value="2" {if $slave->run_mode == 2}selected{/if}>高效</option>
												</select>
										  </label>
									  </div>


										<a class="btn btn-brand-accent" id="requestOn" {if $slave!=0}disabled{/if} >开机</a>
										<a class="btn btn-brand" id="requestOff" {if $slave==0}disabled{/if}>关机</a>
										<a class="btn btn-brand-accent" id="requestEdit" {if $slave==0}disabled{/if}>更改设置</a>
									</div>

								</div>
							</div>
						</div>
					</div>



					{include file='dialog.tpl'}
				</div>
			</section>
		</div>
	</main>







{include file='userfooter.tpl'}

<script>
    $(document).ready(function () {
        $("#requestOn").click(function () {
            $.ajax({
                type: "POST",
                url: "/slave/on",
                dataType: "json",
                data: {

                },
                success: function (data) {
                    if (data.ret) {
                        $("#result").modal();
												$("#msg").html(data.msg);
                    } else {
                        $("#result").modal();
												$("#msg").html(data.msg);
                    }
                },
                error: function (jqXHR) {
                    $("#result").modal();
										$("#msg").html(data.msg+"     出现了一些错误。");
                }
            })
        })
    })
</script>

<script>
    $(document).ready(function () {
        $("#requestOff").click(function () {
            $.ajax({
                type: "POST",
                url: "/slave/off",
                dataType: "json",
                data: {

                },
                success: function (data) {
                    if (data.ret) {
                        $("#result").modal();
												$("#msg").html(data.msg);
                    } else {
                        $("#result").modal();
												$("#msg").html(data.msg);
                    }
                },
                error: function (jqXHR) {
                    $("#result").modal();
										$("#msg").html(data.msg+"     出现了一些错误。");
                }
            })
        })
    })
</script>

<script>
    $(document).ready(function () {
        $("#requestEdit").click(function () {
					if (!/^\\d+$/.test($("#target_temp").val()) || $("#target_temp").val() > {$max_temp} || $("#target_temp").val() < {$min_temp}){
						$("#result").modal();
						$("#msg").html("请输入整数且在{$min_temp}到{$max_temp}之间");
					}
					else {
            $.ajax({
                type: "POST",
                url: "/slave/edit",
                dataType: "json",
                data: {
                    target_temp: $("#target_temp").val(),
										fan_speed: $("#fan_speed").val(),
										run_mode: $("#run_mode").val()
                },
                success: function (data) {
                    if (data.ret) {
                        $("#result").modal();
												$("#msg").html(data.msg);
                    } else {
                        $("#result").modal();
												$("#msg").html(data.msg);
                    }
                },
                error: function (jqXHR) {
                    $("#result").modal();
										$("#msg").html(data.msg+"     出现了一些错误。");
                }
            })
					}
        })
    })
</script>
