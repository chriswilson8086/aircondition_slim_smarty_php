<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no, width=device-width" name="viewport">
	<meta name="theme-color" content="#00000">
	<title>{$config["appName"]}</title>

	<!-- css -->
	<link href="/theme/material/css/base.min.css" rel="stylesheet">
	<link href="/theme/material/css/project.min.css" rel="stylesheet">
	<link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- favicon -->
	<!-- ... -->
</head>
<body class="page-brand">
	<header class="header header-transparent header-waterfall ui-header">
		<ul class="nav nav-list pull-left">
			<li>
				<a data-toggle="menu" href="#ui_menu">
					<span class="icon icon-lg">menu</span>
				</a>
			</li>
		</ul>

		<ul class="nav nav-list pull-right">
			<li class="dropdown margin-right">
				<a class="dropdown-toggle padding-left-no padding-right-no" data-toggle="dropdown">
					<span class="access-hide">管理菜单</span>
					<span class="avatar avatar-sm"><img alt="alt text for John Smith avatar" src="{$user->gravatar}"></span>
					</a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li>
							<a class="padding-right-lg waves-attach" href="/slave"><span class="icon icon-lg margin-right">account_box</span>从机</a>
						</li>
						<li>
							<a class="padding-right-lg waves-attach" href="/front"><span class="icon icon-lg margin-right">account_box</span>前台</a>
						</li>
						<li>
							<a class="padding-right-lg waves-attach" href="/airmanager"><span class="icon icon-lg margin-right">account_box</span>空调管理员</a>
						</li>
						<li>
							<a class="padding-right-lg waves-attach" href="/manager"><span class="icon icon-lg margin-right">account_box</span>酒店经理</a>
						</li>
					</ul>


			</li>
		</ul>
	</header>
	<nav aria-hidden="true" class="menu menu-left nav-drawer nav-drawer-md" id="ui_menu" tabindex="-1">
		<div class="menu-scroll">
			<div class="menu-content">
				<a class="menu-logo" href="/"><i class="icon icon-lg">restaurant_menu</i>&nbsp;菜单</a>
				<ul class="nav">
					<li>
						<a  href="/"><i class="icon icon-lg">bookmark_border</i>&nbsp;首页</a>
					</li>
					<li>
						<a  href="/user"><i class="icon icon-lg">person</i>&nbsp;从机面板</a>
					</li>
					<li>
						<a  href="/user"><i class="icon icon-lg">person</i>&nbsp;前台面板</a>
					</li>
					<li>
						<a  href="/user"><i class="icon icon-lg">person</i>&nbsp;空调管理员面板</a>
					</li>
					<li>
						<a  href="/user"><i class="icon icon-lg">person</i>&nbsp;酒店经理面板</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
