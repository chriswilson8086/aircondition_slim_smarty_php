<?php
/* Smarty version 3.1.31, created on 2018-04-01 17:46:36
  from "/www/wwwroot/panel.buptband.com/resources/views/buptband/table/table.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5ac0aa7c333a52_16237402',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '47e2cb0f5229ff7804f96ed204c157cdb0882de8' => 
    array (
      0 => '/www/wwwroot/panel.buptband.com/resources/views/buptband/table/table.tpl',
      1 => 1514493334,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ac0aa7c333a52_16237402 (Smarty_Internal_Template $_smarty_tpl) {
?>
<table id="table_1" class="mdl-data-table" cellspacing="0" width="100%">
  <thead>
    <tr>
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['table_config']->value['total_column'], 'value', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
        <th class="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</th>
      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    </tr>
  </thead>
  <tfoot>
    <tr>
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['table_config']->value['total_column'], 'value', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
        <th class="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</th>
      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    </tr>
  </tfoot>
  <tbody>
  </tbody>
</table>
<?php }
}
