<?php
/* Smarty version 3.1.31, created on 2018-01-12 09:20:22
  from "/www/wwwroot/digital-store.tk/resources/views/zhihu/guest_index.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a580d56a03ac4_83821675',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9a87834518f79152eef0e16ab1f64cc3354f1d06' => 
    array (
      0 => '/www/wwwroot/digital-store.tk/resources/views/zhihu/guest_index.tpl',
      1 => 1515720010,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:guest_header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5a580d56a03ac4_83821675 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:guest_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<div class="zg-wrap zu-main clearFix" role="main">
        <div class="zu-main-content">
            <div class="zu-main-content-inner">
                <div class="zg-section" id="zh-home-list-title">
					<i class="zg-icon zg-icon-feedlist"></i>
                    <a href="/?sort=hot">最热问题</a>
					<i class="zg-icon zg-icon-feedlist"></i>
                    <a href="/?sort=time">最新问题</a>
                </div>
                <div class="zu-main-feed-con">
                    <div id="js-home-feed-list" class="zh-general-list clearFix">
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['shows']->value, 'show');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['show']->value) {
?>
                        <div class="feed-item" id="feedID-1001">
                            <div class="avatar">
                                <a href="/people/<?php echo $_smarty_tpl->tpl_vars['show']->value['true_user']->id;?>
" class="zm-item-link-avatar">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['show']->value['true_user']->gravatar;?>
" class="zm-item-avatar-img">
                                </a>
                            </div>
                            <div class="feed-main">
                                <div class="feed-source">
									<a href="/people/<?php echo $_smarty_tpl->tpl_vars['show']->value['$affect_user']->id;?>
"><?php echo $_smarty_tpl->tpl_vars['show']->value['affect_user']->full_name;?>
</a>
                                    <?php echo $_smarty_tpl->tpl_vars['show']->value['msg'];?>

                                    <span class="time"><?php echo $_smarty_tpl->tpl_vars['show']->value['time'];?>
</span>
                                </div>
                                <div class="feed-content">
                                    <h2>
                                        <a href="/question/<?php echo $_smarty_tpl->tpl_vars['show']->value['question']->id;?>
" class="question-link"><?php echo $_smarty_tpl->tpl_vars['show']->value['question']->name;?>
</a>
                                    </h2>
                                    <div class="entry-body">
									    <?php if (($_smarty_tpl->tpl_vars['show']->value['type'] < 3)) {?>
                                        <div class="zm-item-vote">
                                            <a class="zm-item-vote-count"><?php echo $_smarty_tpl->tpl_vars['show']->value['info']->vote_count;?>
</a>
                                        </div>
										<?php }?>
                                        <div class="zm-item-answer-author-info">
                                            <span class="summary-wrapper">
                                                <span class="author-link-line">
                                                    <a class="author-link" href="/people/<?php echo $_smarty_tpl->tpl_vars['show']->value['true_user']->id;?>
"><?php echo $_smarty_tpl->tpl_vars['show']->value['true_user']->full_name;?>
</a>
                                                </span>
                                               
                                            </span>
                                        </div>
                                        <div class="zm-item-rich-text expandable">
                                            <div style="display: none;"><?php echo $_smarty_tpl->tpl_vars['show']->value['info']->content;?>
</div>
                                            <div class="zh-summary summary clearFix" style="display: block;">
                                                <?php echo $_smarty_tpl->tpl_vars['show']->value['info']->contentFirst();?>
...
                                                <a href="javascript:;" class="toggle-expand">显示全部</a>
                                            </div>
                                            <p class="expanded-visible">
                                                <a class="answer-date-link meta-item" href="/question/<?php echo $_smarty_tpl->tpl_vars['show']->value['question']->id;
if (($_smarty_tpl->tpl_vars['show']->value['type'] < 3)) {?>#<?php echo $_smarty_tpl->tpl_vars['show']->value['info']->id;
}?>">编辑于 <?php echo $_smarty_tpl->tpl_vars['show']->value['info']->update_date;?>
</a>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="feed-meta">
                                        <div class="zm-item-meta clearFix">
                                            <div class="zm-meta-panel">
                                                <button class="item-collapse meta-item"><i class="z-icon-fold"></i>收起</button>
                                            </div>
										</div>
									</div>
								</div>
							</div>                                          
                        </div> 
                  <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                  <?php echo $_smarty_tpl->tpl_vars['questions']->value->render();?>

                    </div>
                </div>
            </div>
        </div>
        <div class="zu-main-sidebar">
            <div class="zm-side-section">
                <div class="zm-side-section-inner">
                    <div class="zm-side-section-group">
                        <ul class="zm-side-nav">
                            <li class="zm-side-nav-li">
                                <a href="/?sort=code" class="zm-side-nav-link">
                                    <i class="zg-icon zg-icon-sidenav-fav"></i>
                                    编程类
                                </a>
                            </li>
                            <li class="zm-side-nav-li">
                                <a href="/?sort=history" class="zm-side-nav-link">
                                    <i class="zg-icon zg-icon-sidenav-fav"></i>
                                    历史类
                                </a>
                            </li>
							<li class="zm-side-nav-li">
                                <a href="/?sort=politics" class="zm-side-nav-link">
                                    <i class="zg-icon zg-icon-sidenav-fav"></i>
                                    政治类
                                </a>
                            </li>
							<li class="zm-side-nav-li">
                                <a href="/?sort=tech" class="zm-side-nav-link">
                                    <i class="zg-icon zg-icon-sidenav-fav"></i>
                                    科技类
                                </a>
                            </li>
							<li class="zm-side-nav-li">
                                <a href="/?sort=music" class="zm-side-nav-link">
                                    <i class="zg-icon zg-icon-sidenav-fav"></i>
                                    音乐类
                                </a>
                            </li>
							<li class="zm-side-nav-li">
                                <a href="/?sort=movie" class="zm-side-nav-link">
                                    <i class="zg-icon zg-icon-sidenav-fav"></i>
                                    电影类
                                </a>
                            </li>
							<li class="zm-side-nav-li">
                                <a href="/?sort=game" class="zm-side-nav-link">
                                    <i class="zg-icon zg-icon-sidenav-fav"></i>
                                    游戏类
                                </a>
                            </li>
							<li class="zm-side-nav-li">
                                <a href="/?sort=novel" class="zm-side-nav-link">
                                    <i class="zg-icon zg-icon-sidenav-fav"></i>
                                    文学类
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>

<?php $_smarty_tpl->_subTemplateRender('file:footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
