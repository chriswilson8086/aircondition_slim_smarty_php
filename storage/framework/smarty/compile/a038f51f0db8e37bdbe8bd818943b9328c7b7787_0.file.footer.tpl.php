<?php
/* Smarty version 3.1.31, created on 2018-01-17 10:56:58
  from "/www/wwwroot/digital-store.tk/resources/views/zhihu/footer.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a5ebb7acb5b74_18487453',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a038f51f0db8e37bdbe8bd818943b9328c7b7787' => 
    array (
      0 => '/www/wwwroot/digital-store.tk/resources/views/zhihu/footer.tpl',
      1 => 1516157816,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a5ebb7acb5b74_18487453 (Smarty_Internal_Template $_smarty_tpl) {
?>

<div class="zh-footer" id="zh-footer">
                <div class="zh-footer-inner zg-warp">
                    <ul>
                        <li><a href="/"></a></li>
                        <li><a href="/">依然可以与这个世界分享知识</a></li>
                      	<li><a href="/">2018 假的知乎</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <?php echo '<script'; ?>
 type="text/javascript" src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="/theme/zhihu/main.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="/theme/zhihu/people.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 src="/theme/window/window.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="/theme/wangeditor/bootstrap.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
 type="text/javascript" src="/theme/wangeditor/wangEditor.min.js"><?php echo '</script'; ?>
>
    <?php echo '<script'; ?>
>
function displayQuestion() {
    $('.modal-dialog-bg').css('display', 'block');
    $('.modal-wrapper').removeClass('hidden');
}
function refresh(){
    location=location;
}

function submitAjax(button, url, getJson,post) {
    $(button).click(function () {
        $.ajax({
            async: false,
            type: "POST",
            url: url,
            dataType: "json",
            data: getJson(),
            success: function (data) {
                if (data.ret == 1 && data.url !== undefined)
                    window.location.href = data.url;

                win.alert('', data.msg);
            },
            error: function (jqXHR) {
                $("#result").modal();
                $("#msg").html(data.msg + "     出现了一些错误。");
            }
        });
        post();
    })
};
function initEditor(div, content) {
    var editor = new window.wangEditor(div);
    editor.customConfig.uploadImgShowBase64 = true;
  	editor.customConfig.menus = [
       'head',  // 标题
        'bold',  // 粗体
        'italic',  // 斜体
        'underline',  // 下划线
        'strikeThrough',  // 删除线
        'foreColor',  // 文字颜色
        'backColor',  // 背景颜色
        'link',  // 插入链接
        'list',  // 列表
        'justify',  // 对齐方式
        'quote',  // 引用
        'image',  // 插入图片
        'video',  // 插入视频
        'code',  // 插入代码
        'undo',  // 撤销
        'redo'  // 重复
    ]
    editor.create();
    editor.txt.html(content);
    return editor;
};

function questionJson() {
    return {
        "name": $("#questionName").val(),
        "content": questionEditor.txt.html(),
        "type": $("#topic").val()
    };
};

function answerJson() {
    return {
        "content": answerEditor.txt.html(),
    };
};

var questionEditor, answerEditor;
$(document).ready(function () {

    questionEditor = initEditor('#editorQuestion', '问题说明（可选）');
    if (window.location.href.search("question") != -1) {
        answerEditor = initEditor('#editorAnswer', '<p>写回答是一种怎样的体验...</p>');
    }
    submitAjax("#submitQuestion", "/ask", questionJson,(function (){}));
    submitAjax("#submitAnswer", window.location.href + "/answer", answerJson,refresh);
  	$(".zu-top-add-question").click(function () {
       $("#submitQuestion").css('display', 'block');
      	$("#updateQuestion").css('display', 'none');
    });
  	$(".zu-top-add-question").click(function () {
       $("#submitQuestion").css('display', 'block');
      	$("#updateQuestion").css('display', 'none');
    });
  	$("#editQuestion").click(function () {
       $("#submitQuestion").css('display', 'none');
      	$("#updateQuestion").css('display', 'block');
    });
});

function getAjax(url){
    var content;
    $.ajax({
        async: false,
        dataType:"json",
        type: "POST",
        url: window.location.href + url,
        success: function (data) {
            content=data;
        }
    });
    return content;
}
$('#editQuestion').click(function () {
    var question=getAjax("/showask");
    $("#questionName").val(question["name"]);
    questionEditor.txt.html(question["content"]);
    $("#topic").val(question["type"]);
  	submitAjax("#updateQuestion", window.location.href +"/updateask", questionJson,refresh);
    displayQuestion();
})
$('#editAnswer').click(function () {
    var answer=getAjax("/showanswer");
    answerEditor.txt.html(answer["content"]);
  	submitAjax("#updateAnswer", window.location.href + "/updateanswer", answerJson,refresh);
    $('.zh-question-answer-form-wrap').css('display', 'block');
    document.body.scrollTop = document.body.scrollHeight;
})
<?php echo '</script'; ?>
>
	
    <div></div>
</body>
</html><?php }
}
