<?php
/* Smarty version 3.1.31, created on 2018-01-13 04:44:48
  from "/www/wwwroot/digital-store.tk/resources/views/zhihu/guest_question.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a591e40ebf503_05829671',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'df17e6fc1147281bd7002fed06e9aa805e48c9ec' => 
    array (
      0 => '/www/wwwroot/digital-store.tk/resources/views/zhihu/guest_question.tpl',
      1 => 1515789875,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:guest_header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5a591e40ebf503_05829671 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:guest_header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <div class="zg-wrap zu-main clearFix" role="main">
        <div class="zu-main-content">
            <div class="zu-main-content-inner">
                <div class="zm-tag-editor zg-section">
                    <div class="zm-tag-editor-labels zg-clear">
                        <a class="zm-item-tag" href="/sort?=<?php echo $_smarty_tpl->tpl_vars['question']->value->type;?>
"><?php echo $_smarty_tpl->tpl_vars['type']->value;?>
</a>
                    </div>
                </div>
                <div id="zh-question-title" class="zm-editable-status-normal">
                    <h2 class="zm-item-title">
                        <span><?php echo $_smarty_tpl->tpl_vars['question']->value->name;?>

                        </span>
                    </h2>
                </div>
                <div id="zh-question-detail">
                    <div>
                        <?php echo $_smarty_tpl->tpl_vars['question']->value->content;?>

                    </div>
                </div>
                <div class="zh-answer-title">
                    <div id="zh-answer-filter" class="sorter">
                        <span class="lbl">默认排序</span>
                        <a class="lbl" href="?sort=update_date">按时间排序</a>
						<a class="lbl" href="?sort=vote_count">按赞同排序</a>
                        <i class="zg-icon zg-icon-double-arrow"></i>
                    </div>
                    <h3 data-num="<?php echo $_smarty_tpl->tpl_vars['question']->value->answer_count;?>
" id="zh-question-answer-num"><?php echo $_smarty_tpl->tpl_vars['question']->value->answer_count;?>
 个回答</h3>
                </div>
                <div id="zh-question-answer-wrap" class="zh-question-answer-wrapper">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['shows']->value, 'show');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['show']->value) {
?>
                    <div class="zm-item-answer zm-item-expanded">
                        <div class="zm-votebar">
                                <span class="count"><?php echo $_smarty_tpl->tpl_vars['show']->value['answer']->vote_count;?>
</span>
                        </div>
                        <div class="answer-head">
                            <div class="zm-item-answer-author-info">
                                <a class="zm-item-link-avatar">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['show']->value['answer_user']->gravatar;?>
" class="Avatar">
                                </a>
                                <span class="summary-wrapper">
                                    <span class="author-link-line">
                                        <a class="author-link" href="/people/<?php echo $_smarty_tpl->tpl_vars['show']->value['answer_user']->id;?>
"><?php echo $_smarty_tpl->tpl_vars['show']->value['answer_user']->full_name;?>
</a>
                                    </span>
                                </span>
                            </div>
                            <div class="zm-item-vote-info">
                                <span class="voters text">
                                    <a href="#" class="more text">
                                        <span class="js-voteCount"><?php echo $_smarty_tpl->tpl_vars['show']->value['answer']->vote_count;?>
</span>&nbsp;个赞同
                                    </a>
                                </span>
                            </div>
                        </div>
                        <div class="zm-item-rich-text expandable">
                            <div class="zh-summary clearFix" style="display: none;"></div>
                            <div class="zm-editable-content clearFix"><?php echo $_smarty_tpl->tpl_vars['show']->value['answer']->content;?>

                            </div>
                        </div>
                        <div class="zm-item-meta answer-action">
                            <div class="zm-meta-panel">
                                <a class="answer-date-link meta-item">更新于 <?php echo $_smarty_tpl->tpl_vars['show']->value['answer']->update_date;?>
</a>
                                <a href="javascript:;" class="toggle-comment meta-item" num="<?php echo $_smarty_tpl->tpl_vars['show']->value['answer']->reply_count;?>
"><i class="z-icon-comment"></i><?php echo $_smarty_tpl->tpl_vars['show']->value['answer']->reply_count;?>
&nbsp;条评论</a>
                                <a href="#" class="js-report meta-item">作者保留权利</a>
                            </div>
                            <div class="comment-app-holder" style="display: none">
                                <div class="_commentBox-bordered-3Fo">
                                    <i class="icon icon-spike"></i>
                                    <div class="_commentBox-list">
									<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['show']->value['replys'], 'reply');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['reply']->value) {
?>
                                        <div class="_commentBox-root-PQNS">
                                            <a href="/people/<?php echo $_smarty_tpl->tpl_vars['reply']->value['reply_user']->id;?>
" class="_commentBox-avatarLink">
                                                <img class="Avatar Avatar-s" src="<?php echo $_smarty_tpl->tpl_vars['reply']->value['reply_user']->gravatar;?>
">
                                            </a>
                                            <div class="_commentItem_body">
                                                <div class="_commentItem-header"><a href="/people/<?php echo $_smarty_tpl->tpl_vars['reply']->value['reply_user']->id;?>
"><?php echo $_smarty_tpl->tpl_vars['reply']->value['reply_user']->full_name;?>
</a></div>
                                                <div class="_commentItem-content" id ="<?php echo $_smarty_tpl->tpl_vars['reply']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['reply']->value['reply']->content;?>
</div>
                                                <div class="_commentItem-footer">
                                                    <time><?php echo $_smarty_tpl->tpl_vars['reply']->value['reply']->create_date;?>
</time> 
                                                </div>
                                            </div>
                                        </div>
									<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    </div>
                                    <div class="_commentForm-border">
                                        <input type="text" class="_inputBox_root" id="<?php echo $_smarty_tpl->tpl_vars['show']->value['answer']->id;?>
" placeholder="写下你的评论...">
                                        <div class="zm-comment zg-clear">
                                            <a class="zg-right zg-btn-blue" onClick="reply(<?php echo $_smarty_tpl->tpl_vars['show']->value['answer']->id;?>
)">评论</a>
                                            <a href="javascript:;" class="zm-comment-cancel">关闭</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

					<?php echo $_smarty_tpl->tpl_vars['answers']->value->render();?>

			    </div>
            </div>
        </div>
        <div class="zu-main-sidebar">
            <div class="zm-side-section">
                <div class="zm-side-section-inner">
                    <div class="zh-question-followers-sidebar">
                        <div class="zg-gray-normal">
                            <strong><?php echo $_smarty_tpl->tpl_vars['question']->value->follower_count;?>
</strong>
                            人关注该问题
                        </div>
                    </div>
                </div>
            </div>
            <div class="zm-side-section">
                <div class="zm-side-section-inner">
                    <h3>问题状态</h3>
                    <div class="zg-gray-normal">
                        最近活动于
                        <span class="time"><?php echo $_smarty_tpl->tpl_vars['question']->value->update_date;?>
</span>
                    </div>
                </div>
            </div>   
<?php $_smarty_tpl->_subTemplateRender('file:footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
