<?php
/* Smarty version 3.1.31, created on 2018-06-10 19:57:01
  from "C:\wwwroot\air.com\resources\views\buptband\header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b1d120dde5297_27626102',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '5ee2d7b757a5b1b8e222453f7e6915beb3f0506f' => 
    array (
      0 => 'C:\\wwwroot\\air.com\\resources\\views\\buptband\\header.tpl',
      1 => 1522575738,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5b1d120dde5297_27626102 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no, width=device-width" name="viewport">
	<meta name="theme-color" content="#00000">
	<title><?php echo $_smarty_tpl->tpl_vars['config']->value["appName"];?>
</title>

	<!-- css -->
	<link href="/theme/material/css/base.min.css" rel="stylesheet">
	<link href="/theme/material/css/project.min.css" rel="stylesheet">
	<link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	
	<!-- favicon -->
	<!-- ... -->
</head>
<body class="page-brand">
	<header class="header header-transparent header-waterfall ui-header">
		<ul class="nav nav-list pull-left">
			<li>
				<a data-toggle="menu" href="#ui_menu">
					<span class="icon icon-lg">menu</span>
				</a>
			</li>
		</ul>
		
		<ul class="nav nav-list pull-right">
			<li class="dropdown margin-right">
				<a class="dropdown-toggle padding-left-no padding-right-no" data-toggle="dropdown">
				<?php if ($_smarty_tpl->tpl_vars['user']->value->isLogin) {?>
					<span class="access-hide"><?php echo $_smarty_tpl->tpl_vars['user']->value->user_name;?>
</span>
					<span class="avatar avatar-sm"><img alt="alt text for John Smith avatar" src="<?php echo $_smarty_tpl->tpl_vars['user']->value->gravatar;?>
"></span>
					</a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li>
							<a class="padding-right-lg waves-attach" href="/user/"><span class="icon icon-lg margin-right">account_box</span>用户中心</a>
						</li>
						<li>
							<a class="padding-right-lg waves-attach" href="/logout"><span class="icon icon-lg margin-right">exit_to_app</span>登出</a>
						</li>
					</ul>
				<?php } else { ?>
					<span class="access-hide">未登录</span>
					<span class="avatar avatar-sm"><img alt="alt text for John Smith avatar" src="/theme/material/images/users/avatar-001.jpg"></span>
					</a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li>
							<a class="padding-right-lg waves-attach" href="/auth/login"><span class="icon icon-lg margin-right">account_box</span>登录</a>
						</li>
						<li>
							<a class="padding-right-lg waves-attach" href="/auth/register"><span class="icon icon-lg margin-right">pregnant_woman</span>注册</a>
						</li>
					</ul>
				<?php }?>
					
			</li>
		</ul>
	</header>
	<nav aria-hidden="true" class="menu menu-left nav-drawer nav-drawer-md" id="ui_menu" tabindex="-1">
		<div class="menu-scroll">
			<div class="menu-content">
				<a class="menu-logo" href="/"><i class="icon icon-lg">restaurant_menu</i>&nbsp;菜单</a>
				<ul class="nav">
					<li>
						<a  href="/"><i class="icon icon-lg">bookmark_border</i>&nbsp;首页</a>
					</li>
					<?php if ($_smarty_tpl->tpl_vars['user']->value->isLogin) {?>
					<li>
						<a  href="/user"><i class="icon icon-lg">person</i>&nbsp;用户中心</a>
					</li>
					<li>
						<a  href="/logout"><i class="icon icon-lg">call_missed_outgoing</i>&nbsp;退出</a>
					</li>
					<?php } else { ?>
					<li>
						<a  href="/auth/login"><i class="icon icon-lg">vpn_key</i>&nbsp;登录</a>
					</li>
					<li>
						<a  href="/auth/register"><i class="icon icon-lg">pregnant_woman</i>&nbsp;注册</a>
					</li>
					<?php }?>
				</ul>
			</div>
		</div>
	</nav>
<?php }
}
