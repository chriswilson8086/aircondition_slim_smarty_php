<?php
/* Smarty version 3.1.31, created on 2018-01-14 06:53:19
  from "/www/wwwroot/digital-store.tk/resources/views/zhihu/admin/main.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a5a8ddfe8bf85_65370694',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bb15d4f7c8125f46d3e7ca0df13e16933b657598' => 
    array (
      0 => '/www/wwwroot/digital-store.tk/resources/views/zhihu/admin/main.tpl',
      1 => 1515883997,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a5a8ddfe8bf85_65370694 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta content="IE=edge" http-equiv="X-UA-Compatible">
	<meta content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no, width=device-width" name="viewport">
	<meta name="theme-color" content="#0767c8">
	<title><?php echo $_smarty_tpl->tpl_vars['config']->value["appName"];?>
</title>



	<!-- css -->
	<link href="/theme/material/css/base.css" rel="stylesheet">
	<link href="/theme/material/css/project.css" rel="stylesheet">
	<link href="//fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="//cdn.staticfile.org/material-design-lite/1.1.0/material.min.css" rel="stylesheet">
	<link href="//cdn.staticfile.org/datatables/1.10.13/css/dataTables.material.min.css" rel="stylesheet">



	<!-- favicon -->
	<!-- ... -->
</head>

<body class="page-brand">
	<header class="header header-transparent header-waterfall ui-header">
		<ul class="nav nav-list pull-left">
			<div>
				<a data-toggle="menu" href="#ui_menu">
					<span class="icon icon-lg">menu</span>
				</a>
			</div>
		</ul>

		<ul class="nav nav-list pull-right">
			<div class="dropdown margin-right">
				<a class="dropdown-toggle padding-left-no padding-right-no" data-toggle="dropdown">
					<span class="access-hide"><?php echo $_smarty_tpl->tpl_vars['user']->value->user_name;?>
</span>
					<span class="avatar avatar-sm"><img alt="alt text for John Smith avatar" src="<?php echo $_smarty_tpl->tpl_vars['user']->value->gravatar;?>
"></span>
					</a>
				<ul class="dropdown-menu dropdown-menu-right">
					<li>
						<a class="padding-right-lg waves-attach" href="/"><span class="icon icon-lg margin-right">account_box</span>返回首页</a>
					</li>
					<li>
						<a class="padding-right-lg waves-attach" href="/logout"><span class="icon icon-lg margin-right">exit_to_app</span>登出</a>
					</li>
				</ul>

			</div>
		</ul>
	</header>
	<nav aria-hidden="true" class="menu menu-left nav-drawer nav-drawer-md" id="ui_menu" tabindex="-1">
		<div class="menu-scroll">
			<div class="menu-content">
				<a class="menu-logo" href="/"><i class="icon icon-lg">person_pin</i>&nbsp;管理面板</a>
				<ul class="nav">
					<li>
						<ul class="menu-collapse collapse in" id="ui_menu_me">
							<li><a href="/admin/user"><i class="icon icon-lg">supervisor_account</i>&nbsp;用户列表</a></li>
							<li><a href="/admin/question"><i class="icon icon-lg">question_answer</i>&nbsp;问题管理</a></li>
							<li><a href="/admin/answer"><i class="icon icon-lg">question_answer</i>&nbsp;回答管理</a></li>
							<li><a href="/admin/reply"><i class="icon icon-lg">question_answer</i>&nbsp;评论管理</a></li>
						</ul>

						<li><a href="/"><i class="icon icon-lg">person</i>&nbsp;返回首页</a></li>
					</li>



				</ul>
			</div>
		</div>
	</nav>
<?php }
}
