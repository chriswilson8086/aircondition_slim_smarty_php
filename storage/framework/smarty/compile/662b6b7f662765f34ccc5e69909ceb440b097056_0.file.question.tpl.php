<?php
/* Smarty version 3.1.31, created on 2018-01-14 10:48:07
  from "/www/wwwroot/digital-store.tk/resources/views/zhihu/question.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a5ac4e7f243e4_80275965',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '662b6b7f662765f34ccc5e69909ceb440b097056' => 
    array (
      0 => '/www/wwwroot/digital-store.tk/resources/views/zhihu/question.tpl',
      1 => 1515898027,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5a5ac4e7f243e4_80275965 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

    <div class="zg-wrap zu-main clearFix" role="main">
        <div class="zu-main-content">
            <div class="zu-main-content-inner">
                <div class="zm-tag-editor zg-section">
                    <div class="zm-tag-editor-labels zg-clear">
                        <a class="zm-item-tag" href="/sort?=<?php echo $_smarty_tpl->tpl_vars['question']->value->type;?>
"><?php echo $_smarty_tpl->tpl_vars['type']->value;?>
</a>
                    </div>
                </div>
                <div id="zh-question-title" class="zm-editable-status-normal">
                    <h2 class="zm-item-title">
                        <span><?php echo $_smarty_tpl->tpl_vars['question']->value->name;?>

                        </span>
                    </h2>
                </div>
                <div id="zh-question-detail">
                    <div>
                        <?php echo $_smarty_tpl->tpl_vars['question']->value->content;?>

                        <?php if ($_smarty_tpl->tpl_vars['is_mine']->value) {?>
                        <a id="editQuestion" class="zu-edit-button" href="javascript:;" name="edit">
                            <i  class="zu-edit-button-icon"></i>
                            修改
                        </a>
                      	<?php }?>
                    </div>
                </div>
                <div class="zh-answer-title">
                    <div id="zh-answer-filter" class="sorter">
                        <span class="lbl">默认排序</span>
                        <a class="lbl" href="?sort=update_date">按时间排序</a>
						<a class="lbl" href="?sort=vote_count">按赞同排序</a>
                        <i class="zg-icon zg-icon-double-arrow"></i>
                    </div>
                    <h3 data-num="<?php echo $_smarty_tpl->tpl_vars['question']->value->answer_count;?>
" id="zh-question-answer-num"><?php echo $_smarty_tpl->tpl_vars['question']->value->answer_count;?>
 个回答</h3>
                </div>
                <div id="zh-question-answer-wrap" class="zh-question-answer-wrapper">
				<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['shows']->value, 'show');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['show']->value) {
?>
                    <div class="zm-item-answer zm-item-expanded">
                        <div class="zm-votebar">
                            <button class="up <?php if ($_smarty_tpl->tpl_vars['show']->value['is_vote']) {?>pressed<?php }?>" title="<?php if ($_smarty_tpl->tpl_vars['show']->value['is_vote']) {?>取消<?php }?>赞同" onClick="vote(<?php echo $_smarty_tpl->tpl_vars['show']->value['answer']->id;?>
)">
                                <i class="icon vote-arrow"></i>
                                <span class="count"><?php echo $_smarty_tpl->tpl_vars['show']->value['answer']->vote_count;?>
</span>
                            </button>
                        </div>
                        <div class="answer-head">
                            <div class="zm-item-answer-author-info">
                                <a class="zm-item-link-avatar">
                                    <img src="<?php echo $_smarty_tpl->tpl_vars['show']->value['answer_user']->gravatar;?>
" class="Avatar">
                                </a>
                                <span class="summary-wrapper">
                                    <span class="author-link-line">
                                        <a class="author-link" href="/people/<?php echo $_smarty_tpl->tpl_vars['show']->value['answer_user']->id;?>
"><?php echo $_smarty_tpl->tpl_vars['show']->value['answer_user']->full_name;?>
</a>
                                    </span>
                                </span>
                            </div>
                            <div class="zm-item-vote-info">
                                <span class="voters text">
                                    <a href="#" class="more text">
                                        
                                    </a>
                                </span>
                            </div>
                        </div>
                        <div class="zm-item-rich-text expandable">
                            <div class="zh-summary clearFix" style="display: none;"></div>
                            <div class="zm-editable-content clearFix"><?php echo $_smarty_tpl->tpl_vars['show']->value['answer']->content;?>

                            </div>
                        </div>
                        <div class="zm-item-meta answer-action">
                            <div class="zm-meta-panel">
                              	
                                <a class="answer-date-link meta-item">更新于 <?php echo $_smarty_tpl->tpl_vars['show']->value['answer']->update_date;?>
</a>
                                <a href="javascript:;" class="toggle-comment meta-item" num="<?php echo $_smarty_tpl->tpl_vars['show']->value['answer']->reply_count;?>
"><i class="z-icon-comment"></i><?php echo $_smarty_tpl->tpl_vars['show']->value['answer']->reply_count;?>
&nbsp;条评论</a>
                                <a href="#" class="js-report meta-item">作者保留权利</a>
                            </div>
                            <div class="comment-app-holder" style="display: none">
                                <div class="_commentBox-bordered-3Fo">
                                    <i class="icon icon-spike"></i>
                                    <div class="_commentBox-list">
									<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['show']->value['replys'], 'reply');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['reply']->value) {
?>
                                        <div class="_commentBox-root-PQNS">
                                            <a href="/people/<?php echo $_smarty_tpl->tpl_vars['reply']->value['reply_user']->id;?>
" class="_commentBox-avatarLink">
                                                <img class="Avatar Avatar-s" src="<?php echo $_smarty_tpl->tpl_vars['reply']->value['reply_user']->gravatar;?>
">
                                            </a>
                                            <div class="_commentItem_body">
                                                <div class="_commentItem-header"><a href="/people/<?php echo $_smarty_tpl->tpl_vars['reply']->value['reply_user']->id;?>
"><?php echo $_smarty_tpl->tpl_vars['reply']->value['reply_user']->full_name;?>
</a></div>
                                                <div class="_commentItem-content" id ="<?php echo $_smarty_tpl->tpl_vars['reply']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['reply']->value['reply']->content;?>
</div>
                                                <div class="_commentItem-footer">
                                                    <time><?php echo $_smarty_tpl->tpl_vars['reply']->value['reply']->create_date;?>
</time> 
                                                </div>
                                            </div>
                                        </div>
									<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                                    </div>
                                    <div class="_commentForm-border">
                                        <input type="text" class="_inputBox_root" id="<?php echo $_smarty_tpl->tpl_vars['show']->value['answer']->id;?>
" placeholder="写下你的评论...">
                                        <div class="zm-comment zg-clear">
                                            <a class="zg-right zg-btn-blue" onClick="reply(<?php echo $_smarty_tpl->tpl_vars['show']->value['answer']->id;?>
)">评论</a>
                                            <a href="javascript:;" class="zm-comment-cancel">关闭</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

					<?php echo $_smarty_tpl->tpl_vars['answers']->value->render();?>

			    </div>
              
                <div id="zh-question-answer-form-wrap" class="zh-question-answer-form-wrap" style="display: none">
                  <div class="zm-editable-editor-wrap">
                    <div class="zm-answer-form">
                        <div class="zu-answer-form-title">
                          <a href="/people/<?php echo $_smarty_tpl->tpl_vars['user']->value->id;?>
"><?php echo $_smarty_tpl->tpl_vars['user']->value->full_name;?>
</a></div>
                      <div style="max-width:800px;">
                        <div id="editorAnswer" style="border:1px solid gray;min-height:240px"></div>
                      </div>
                      
                    </div>
                    <div class="zm-command">
                        <span class="zg-right">
                          <?php if ($_smarty_tpl->tpl_vars['is_answered']->value) {?>
                          <a class="submit-button zg-btn-blue" id="updateAnswer" href="javascript:;">发布回答</a>
                          <?php } else { ?>
                          <a class="submit-button zg-btn-blue" id="submitAnswer" href="javascript:;">发布回答</a>
                      	  <?php }?>
                      </span>
                      </div>
                  </div>
                </div>
              
            </div>
        </div>
        <div class="zu-main-sidebar">
            <div class="zm-side-section">
                <div class="zm-side-section-inner">
                    <?php if ($_smarty_tpl->tpl_vars['is_follow']->value) {?><button class="follow-button zg-follow zg-btn-white" onClick="follow(<?php echo $_smarty_tpl->tpl_vars['question']->value->id;?>
)" id="follow">取消关注问题</button>
                  	<?php } else { ?><button class="follow-button zg-follow zg-btn-blue" onClick="follow(<?php echo $_smarty_tpl->tpl_vars['question']->value->id;?>
)" id="follow">关注问题</button><?php }?>
                  	<button class="follow-button zg-follow zg-btn-blue" id="editAnswer"><?php if ($_smarty_tpl->tpl_vars['is_answered']->value) {?>修改<?php }?>回答</button>
                    <div class="zh-question-followers-sidebar">
                        <div class="zg-gray-normal">
                            <strong><?php echo $_smarty_tpl->tpl_vars['question']->value->follower_count;?>
</strong>
                            人关注该问题
                        </div>
                    </div>
                </div>
            </div>
            <div class="zm-side-section">
                <div class="zm-side-section-inner">
                    <h3>问题状态</h3>
                    <div class="zg-gray-normal">
                        最近活动于
                        <span class="time"><?php echo $_smarty_tpl->tpl_vars['question']->value->update_date;?>
</span>
                    </div>
                </div>
            </div>
    <?php echo '<script'; ?>
>

function follow(id){
		$.ajax({
          	async:false,
			type:"POST",
			url:"/followquestion",
			dataType:"json",
			data:{
				id: id
			},
			success:function(data){
				if(data.ret){
					win.alert('', data.msg); 	
				}else{
					win.alert('', data.msg); 	
				}
			},
			error:function(jqXHR){
				win.alert('', data.msg);
			}
		});
  		location=location;
}
function vote(id){
		$.ajax({
          	async:false,
			type:"POST",
			url:"/vote",
			dataType:"json",
			data:{
				id: id
			},
			success:function(data){
				if(data.ret){
					win.alert('', data.msg); 
				}else{
					win.alert('', data.msg); 
				}
			},
			error:function(jqXHR){
				win.alert('', data.msg); 
			}
		});
  		//location=location;
}
      
function reply(id){
     inputbox = "#" + id;
		$.ajax({
			type:"POST",
			url:"/reply",
			dataType:"json",
			data:{
				id: id,
                content: $(inputbox).val()
			},
			success:function(data){
				if(data.ret){
					win.alert('', data.msg);
                    window.location.href =data.url;
				}else{
					win.alert('', data.msg); 
				}
			},
			error:function(jqXHR){
				win.alert('', data.msg); 
			}
		});
}
<?php echo '</script'; ?>
>      
<?php $_smarty_tpl->_subTemplateRender('file:footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
