<?php
/* Smarty version 3.1.31, created on 2018-01-13 04:36:12
  from "/www/wwwroot/digital-store.tk/resources/views/zhihu/message.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a591c3c325623_93225218',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3d0f3f6e7ea1fa775b0565c26f237d33c46c5139' => 
    array (
      0 => '/www/wwwroot/digital-store.tk/resources/views/zhihu/message.tpl',
      1 => 1515789368,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5a591c3c325623_93225218 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<div class="zg-wrap zu-main clearFix" role="main">
        <div class="zu-main-content">
            <div class="zu-main-content-inner">
                <div class="zg-section" id="zh-home-list-title">
                    <i class="zg-icon zg-icon-feedlist"></i>
                    <a href="/message?sort=not_read">未读消息</a>
					<i class="zg-icon zg-icon-feedlist"></i>
                    <a href="/message?sort=is_read">已读消息</a>
                </div>
                <div class="zu-main-feed-con">
                    <div id="js-home-feed-list" class="zh-general-list clearFix">
					<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['shows']->value, 'show');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['show']->value) {
?>
                        <div class="feed-item" id="feedID-1001">
                        <p><?php echo $_smarty_tpl->tpl_vars['show']->value['message']->create_date;?>
</p><br/>
						<p><a href="/people/<?php echo $_smarty_tpl->tpl_vars['show']->value['message']->from_user_id;?>
"><?php echo $_smarty_tpl->tpl_vars['show']->value['name'];?>
</a><?php echo $_smarty_tpl->tpl_vars['show']->value['msg1'];
if ($_smarty_tpl->tpl_vars['show']->value['message']->type > 1) {?><a href="/question/<?php echo $_smarty_tpl->tpl_vars['show']->value['message']->content;?>
"><?php echo $_smarty_tpl->tpl_vars['show']->value['title'];?>
</a><?php echo $_smarty_tpl->tpl_vars['show']->value['msg2'];
}?></p>
                        </div> 
                  <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

                  <?php echo $_smarty_tpl->tpl_vars['messages']->value->render();?>

                    </div>
                </div>
            </div>
        </div>
        <div class="zu-main-sidebar">
            <div class="zm-side-section">
<?php $_smarty_tpl->_subTemplateRender('file:footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
