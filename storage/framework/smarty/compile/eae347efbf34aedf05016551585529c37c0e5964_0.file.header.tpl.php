<?php
/* Smarty version 3.1.31, created on 2018-01-15 17:00:01
  from "/www/wwwroot/digital-store.tk/resources/views/zhihu/header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a5c6d91f16eb6_10018211',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'eae347efbf34aedf05016551585529c37c0e5964' => 
    array (
      0 => '/www/wwwroot/digital-store.tk/resources/views/zhihu/header.tpl',
      1 => 1516001326,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a5c6d91f16eb6_10018211 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title><?php echo $_smarty_tpl->tpl_vars['config']->value["appName"];?>
</title>
    <link type="text/css" rel="stylesheet" href="/theme/zhihu/main.css" />
    <link rel="stylesheet" type="text/css" href="/theme/wangeditor/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/theme/wangeditor/font-awesome.min.css">
    <!--[if IE]>
      <link rel="stylesheet" type="text/css" href="/theme/wangeditor/fontawesome-4.2.0/css/font-awesome-ie7.min.css">
    <![endif]-->
  	<link href="/theme/window/img.css" rel="stylesheet" >
    <link rel="stylesheet" type="text/css" href="/theme/wangeditor/common.css">
    <link rel="stylesheet" type="text/css" href="/theme/wangeditor/index.css">
    <link href="/theme/window/window.css" rel="stylesheet" >
  	
</head>
<body class="zhi">
    <div class="zu-top">
        <div class="zg-warp clearfix">
            <a href="/" class="zu-top-link-logo">知乎</a>
            <div role="search" id="zh-top_search" class="zu-top-search">
                <form method="get" action="/search" id="zh-top-search-form" class="zu-top-search-form">
                    <input type="text" class="zu-top-search-input" placeholder="搜索你感兴趣的内容...">
                    <button type="submit" class="zu-top-search-button">
                        <span class="sprite-icon-magnifier-dark"></span>
                    </button>
                </form>
            </div>
            <div class="zu-top-nav">
                <ul class="zu-top-nav-ul">
                    <li class="zu-top-nav-li">
                        <a class="zu-top-nav-link" href="/">首页</a>
                    </li>
					
					<?php if ($_smarty_tpl->tpl_vars['user']->value->isAdmin()) {?>
                    <li class="zu-top-nav-li">
                        <a class="zu-top-nav-link" href="/admin/user">管理</a>
                    </li>
					<?php }?>
                 	 <li class="zu-top-nav-li">
                  		<button class="zu-top-add-question">提问</button>
                     </li>
                </ul>
            </div>
            <div class="zu-top-profile">
                <a href="/people/<?php echo $_smarty_tpl->tpl_vars['user']->value->id;?>
" class="zu-top-profile-name">
                    <span class="name"><?php echo $_smarty_tpl->tpl_vars['user']->value->full_name;?>
</span>
                    <img class="avatar" src="<?php echo $_smarty_tpl->tpl_vars['user']->value->gravatar;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['user']->value->full_name;?>
">
                    <?php if ($_smarty_tpl->tpl_vars['user']->value->countMessage()) {?>
                    <span class="zu-top-pm-count zg-noti-number"><?php echo $_smarty_tpl->tpl_vars['user']->value->countMessage();?>
</span>
                    <?php }?>
                </a>
                <ul class="top-nav-dropDown">
                    <li>
                        <a href="/people/<?php echo $_smarty_tpl->tpl_vars['user']->value->id;?>
"><i class="zg-icon zg-icon-home"></i>我的主页</a>
                    </li>
                    <li>
                        <a href="/message"><i class="zg-icon zg-icon-pm"></i>消息</a>
                    </li>
                    <li>
                        <a href="/settings"><i class="zg-icon zg-icon-dd-settings"></i>设置</a>
                    </li>
                    <li>
                        <a href="/logout"><i class="zg-icon zg-icon-logout"></i>退出</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
  	<div class="modal-dialog-bg" style="opacity: 0.5; width: 100%; height: 100%; display: none"></div>
	<div class="modal-wrapper hidden">
  <div class="modal-dialog absolute-position" style="width: 680px">
    <div class="modal-dialog-title">
      <span class="modal-dialog-title-text">提问</span>
      <span class="modal-dialog-title-close"></span>
    </div>
    <div class="modal-dialog-content">
      <div class="zh-add-question-form">
        <div class="js-add-question-splash">
          <input type="text" id="questionName" class="zg-from-text-input" placeholder="写下你的问题">
          
         
          <div class="add-question-section-title"><br/></div>
          <div class="zm-command">
            <div style="max-width:800px;text-align:left;">
              <div id="editorQuestion" style="border:1px solid gray;min-height:100px"></div>
            </div>
            <br/>
            <label class="zg-left copyright-box">
                                <select name="copyright" id="topic">
                                    <option value="code">编程类</option>
                                    <option value="history">历史类</option>
                                    <option value="politics">政治类</option>
                                    <option value="tech">科技类</option>
                                    <option value="music">音乐类</option>
                                    <option value="movie">电影类</option>
                                    <option value="game">游戏类</option>
                                    <option value="novel">文学类</option>
                                </select>
                            </label>
            <span class="zg-right">
              <?php if ($_smarty_tpl->tpl_vars['is_mine']->value) {?><a class="submit-button zg-btn-blue" id="updateQuestion" href="javascript:;" style="display:none">发布</a><?php }?>
              <a class="submit-button zg-btn-blue" id="submitQuestion" href="javascript:;" style="display:none">发布</a></span>
            <br/>
          </div>
          
        </div>
      </div>
    </div>
  </div>
</div><?php }
}
