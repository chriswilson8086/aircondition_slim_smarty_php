<?php
/* Smarty version 3.1.31, created on 2018-01-12 09:11:41
  from "/www/wwwroot/digital-store.tk/resources/views/zhihu/guest_header.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a580b4db00e96_59825664',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '62c325ed32ae4a7314c8b5a405f04160ac54846d' => 
    array (
      0 => '/www/wwwroot/digital-store.tk/resources/views/zhihu/guest_header.tpl',
      1 => 1515719497,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a580b4db00e96_59825664 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <title><?php echo $_smarty_tpl->tpl_vars['config']->value["appName"];?>
</title>
    <link type="text/css" rel="stylesheet" href="/theme/zhihu/main.css" />
    <link rel="stylesheet" type="text/css" href="/theme/wangeditor/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/theme/wangeditor/font-awesome.min.css">
    <!--[if IE]>
      <link rel="stylesheet" type="text/css" href="/theme/wangeditor/fontawesome-4.2.0/css/font-awesome-ie7.min.css">
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="/theme/wangeditor/common.css">
    <link rel="stylesheet" type="text/css" href="/theme/wangeditor/index.css">
    <link href="/theme/window/window.css" rel="stylesheet" />

</head>
<body class="zhi">
    <div class="zu-top">
        <div class="zg-warp clearfix">
            <a href="/" class="zu-top-link-logo">知乎</a>
            <div role="search" id="zh-top_search" class="zu-top-search">
                <form method="get" action="/search" id="zh-top-search-form" class="zu-top-search-form">
                    <input type="text" class="zu-top-search-input" placeholder="搜索你感兴趣的内容...">
                    <button type="submit" class="zu-top-search-button">
                        <span class="sprite-icon-magnifier-dark"></span>
                    </button>
                </form>
            </div>
            <div class="zu-top-nav">
                <ul class="zu-top-nav-ul">
                    <li class="zu-top-nav-li">
                        <a class="zu-top-nav-link" href="/">首页</a>
                    </li>
					<li class="zu-top-nav-li">
                        <a class="zu-top-nav-link" href="/auth">登陆</a>
                    </li>
                </ul>
            </div>
            <div class="zu-top-profile">
                <a href="/auth" class="zu-top-profile-name">
                    <span class="name">临时用户</span>
                    <img class="avatar" src="/theme/zhihu/img/avatar-001.jpg">
                </a>
            </div>
        </div>
    </div><?php }
}
