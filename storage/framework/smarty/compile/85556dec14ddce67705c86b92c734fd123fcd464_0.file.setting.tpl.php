<?php
/* Smarty version 3.1.31, created on 2018-01-17 15:28:32
  from "/www/wwwroot/digital-store.tk/resources/views/zhihu/setting.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a5efb2035a6e1_62070847',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '85556dec14ddce67705c86b92c734fd123fcd464' => 
    array (
      0 => '/www/wwwroot/digital-store.tk/resources/views/zhihu/setting.tpl',
      1 => 1516174109,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a5efb2035a6e1_62070847 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="zh-CN" style="height: 100%; display: block">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $_smarty_tpl->tpl_vars['config']->value["appName"];?>
</title>
    <style type="text/css">
        html{
            padding: 0;
            margin: 0 ;
            height: 100% !important;
        }
        body{
            margin: 0;
            height: 100%;
            color: #555;
            margin: 0;
            padding: 0;
            width: 100%;
            display: flex;
            align-items: center;
            font-size: 14px;
            background: #f7fafc;
        }
        input, button{
            outline: none;
        }
        a{
            text-decoration: none;
        }
        .logo{
            margin: 0 auto;
            width: 160px;
            height: 74px;
            background: url("img/logo.png") no-repeat;
            text-align: center;
            background-size: contain;
        }
        .subtitle{
            margin: 30px 0 20px;
            font-weight: 400;
            font-size: 18px;
            line-height: 1;
            text-align: center;
        }
        .index-main{
            width: 100%;
            height: 100%;
            text-align: center;
        }
        .index-main:before{
            content: "";
            display: inline-block;
            height: 100%;
            vertical-align: middle;
        }
        .index-main-body{
            padding: 0;
            display: inline-block;
            vertical-align: middle;
            width: 300px;
            min-height: 590px;
            margin: auto;
        }
        .index-header{
            text-align: center;
        }
        .index-main-body p{
            width: inherit;
            text-align: center;
            opacity: 0.5;
        }
        .sign-nav{
            position: relative;
            text-align: center;
            font-size: 14px;           
            color: black;
            margin-bottom: 10px;
        }
        .sign-nav a{
            float: left;
            display: inline-block;
            line-height: 35px;
            text-decoration: none;
            opacity: 0.6;
            transition: opacity 0.5s;
            color: inherit;
        }
        .sign-nav a:hover{
            opacity: 1;
        }
        .sign-nav a.active{
            opacity: 1;
            color: #0f88eb;
        }
        /* .nav-slider-bar{
            position: absolute;
            margin: 0 0em;
            background: #0f88eb;
            height: 2px;
            width: 4.5em;
            left: 0;
            bottom: 0;
            transition: left .15s linear;
        } */
        .sign-nav[data-index="1"] .nav-slider-bar{
            left: 4em;
        }
        .group-input{
            border: 1px solid #d5d5d5;
            border-radius: 3px;
        }
        .input-wrapper{
            margin: 0;
            position: relative;
        }
        div.input-wrapper input{
            width: 100%;
            padding: 1em 0.8em;
            line-height: 19px;
            box-sizing: border-box;
            color: #555;
            border: 0;
        }
        .input-wrapper + .input-wrapper{
            border-top: 1px solid #e8e8e8;
        }
        .button-wrapper{
            margin-top: 18px;
            display: block;
        }
        .sign-btn{
            background: #0f88eb;
            line-height: 41px;
            color: white;
            border-radius: 3px;
            width: 100%;
            font-size: 15px;
            text-align: center;
            cursor: pointer;
            border: 0;
        }
        .signin-misc-wrapper, .weibo-signup-wrapper{
            margin-top: 18px;
            display: block;
        }
        .signin-misc-wrapper .remember-me{
            float: left;
        }
        .signin-misc-wrapper .unable-login{
            float: right;
            color: inherit;
        }
        .clearFix:after, .clearFix:before{
            content: "";
            display: block;
            clear: both;
            visibility: hidden;
            line-height: 0;
            height: 0;
            font-size: 0;
        }
        .js-toggle-sns-btn{
            cursor: pointer;
            float: left;
            display: block;
        }
        .sns-btn > a{
            margin: 0 25px 0 0;
        }
        .weibo-signup-wrapper a{
            opacity: 0.7;
            -webkit-transition: opacity 0.15s;
        }
        .weibo-signup-wrapper a:hover{
            opacity: 1;
        }
        [class ^='sprite-sns-'] {
            background: url("img/sns.png") no-repeat;
            display: inline-block;
            width: 20px;
            height: 18px;
        }
        .sprite-sns-wechat{
            background-position: -56px -20px;
        }
        .sprite-sns-weibo{
            background-position: 0px -44px;
        }
        .sprite-sns-qq{
            background-position: -56px 0px;
        }
        .sns-btn{
            display: inline-block;
            opacity: 0;
            transform: translateX(-20px);
            transition: opacity .25s ease-out, transform 0.25s ease-out, -webkit-transform 0.25s ease-out;
            visibility: hidden;
        }
        .sns-visible{
            opacity: 1;
            transform: none;
        }

        .captcha-module img{
            position: absolute;
            right: 0.5em;
            margin-top: -19px;
            top: 50%;
            width: 100px;
            height: 38px;
        }

        .footer{
            position: fixed;
            width: 100%;
            left: 0;
            bottom: 0;
            font-size: 12px;
            line-height: 42px;
            text-align: center;
        }
        .footer a, .footer span{
            color: #aebdc9;
        }
        .dot{
            margin: 0 3px;
        }
        .profile-edit{
            font-size: 16px;
        }
        .intro-edit, .password-change{
            font-size: 16px;
            margin-left: 20px;
        }
        .introduction textarea{
            width:300px; 
            height:200px; 
            border:solid 1px #0f88eb; 
            border-radius:5px; 
            resize:none;
            outline:none;
            font-family: 微软雅黑;
        }
        .introduction textarea::-webkit-input-placeholder{
            text-align: center;
            line-height: 30px;
        }
    </style>
</head>
<body>
    <div class="index-main">
        <div class="index-main-body">
            <div class="index-header">
                <h1 class="logo"></h1>
                <h2 class="subtitle">与世界分享你的知识，经验和见解</h2>
            </div>
            <div class="sign-flow">
                <div class="sign-nav" data-index="0">
                    <div class="nav-slider" style="display: inline-block; position: relative;">
                        <a class="profile-edit">资料编辑</a>
                        <a class="intro-edit">介绍编辑</a>
                        <a class="password-change">密码修改</a>
                        <span class="nav-slider-bar"></span>
                    </div>
                </div>

                <div id="profileEdit" class="profileEdit" style="display: block">
                    <form novalidate="novalidate">
                        <div class="group-input">
                            <div class="name input-wrapper">
                                <input id="full_name" type="text" name="name" placeholder="姓名（不修改请勿填写）" required>
                            </div>
                            <div class="location input-wrapper">
                                <input id="location" type="text" name="location" placeholder="所在地（不修改请勿填写）" required>
                            </div>
                            <div class="job input-wrapper">
                                <input id="job" type="text" name="job" placeholder="工作（不修改请勿填写）" required>
                            </div>
                            <div class="education input-wrapper">
                                <input id="education" type="text" name="education" placeholder="教育程度（不修改请勿填写）" required>
                            </div>

                        </div>
                        <div class="button-wrapper">
                            <button class="sign-btn" id="info" type="button">确定</button>
                        </div>
                    </form>

                </div>

                <div id="introEdit" class="introEdit" style="display: none">
                    <form novalidate="novalidate">
                        <div class="introduction">
                            <textarea type="text" id="personalDesc" placeholder="请输入你的个人描述...（不修改请勿填写）" required></textarea>
                        </div>
                        <div class="button-wrapper">
                            <button class="sign-btn" id="description" type="button">确定</button>
                        </div>
                    </form>

                </div>

                <div id="passwordChange" class="passwordChange" style="display: none">
                    <form novalidate="novalidate">
                        <div class="group-input">
                            <div class="input-wrapper">
                                <input type="password" id="oldPasswd" name="password-origin" placeholder="旧密码" required>
                            </div>
                            <div class="input-wrapper">
                                <input type="password" id="newPasswd" name="password-new" placeholder="新密码" required>
                            </div>
                            <div class="input-wrapper">
                                <input type="password" id="repeatPasswd" name="password-new-again" placeholder="再次输入新密码" required>
                            </div>
                        </div>
                        <div class="button-wrapper">
                            <button class="sign-btn" id="reset_passwd" type="button">确定</button>
                        </div>
                    </form>

                </div>
				<div class="button-wrapper">
                      <button class="sign-btn" onclick="window.location.href='/'">返回主页</button>
                </div>
                

            </div>
        </div>
    </div>
    <div class="footer">
        <span>@假的知乎</span>
        <span class="dot">·</span>
        <a target="_blank" href="#">打死不备案</a>
    </div>
    <canvas id="canvas" style="position: absolute; top: 0; left: 0; z-index: -1"></canvas>
</body>
<?php echo '<script'; ?>
 type="text/javascript" src="https://apps.bdimg.com/libs/jquery/2.1.4/jquery.min.js"><?php echo '</script'; ?>
>
  <link href="/theme/window/window.css" rel="stylesheet" />
  <?php echo '<script'; ?>
 src="/theme/window/window.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
    function stopBubble(e) {
        if(e && e.stopPropagation()){
            e.stopPropagation();
        }
        else{
            window.event.cancelBubble = true;
        }
    }
    var canvas = document.getElementById("canvas");
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    var ctx = canvas.getContext('2d');
    function rand(min, max) {
        return parseInt(Math.random()*(max - min) + min);
    }
    function Round() {
        /*半径*/
        this.r = rand(5, 15);
        var speed = rand(1, 3);
        this.speedX = rand(0, 4) > 2 ? speed: -speed;
        this.speedY = rand(0, 4) > 2 ? speed: -speed;
        var x = rand(this.r, canvas.width - this.r);
        this.x = x < this.r ? this.r : x;
        var y = rand(this.r, canvas.height - this.r);
        this.y = y < this.y ? this.r : y;
    }
    Round.prototype.draw = function () {
        ctx.fillStyle = 'rgba(200, 200, 200, 0.2)';
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.r, 0, 2* Math.PI, true);
        ctx.closePath();
        ctx.fill();
    }
    Round.prototype.move = function () {
        this.x += this.speedX/10;
        if(this.x > canvas.width || this.x < 0){
            this.speedX *= -1;
        }
        this.y += this.speedY/10;
        if(this.y > canvas.height || this.y < 0){
            this.speedY *= -1;
        }
    }
    Round.prototype.links = function () {
        for(var loop = 0; loop < setBall.length; loop++){
            var len = Math.sqrt(((this.x - setBall[loop].x)*(this.x - setBall[loop].x)) + ((this.y-setBall[loop].y)*(this.y-setBall[loop].y)));
            var line = 1/len * 2;
            if(len < 250){
                ctx.beginPath();
                ctx.strokeStyle = 'rgba(0, 0, 0, '+ line + ')';
                ctx.moveTo(this.x, this.y);
                ctx.lineTo(setBall[loop].x, setBall[loop].y);
                ctx.stroke();
                ctx.closePath();
            }
        }
    }
    var setBall = [];
    function init() {
        for(var num = 0; num < 50; num++){
            var obj = new Round();
            obj.draw();
            obj.move();
            setBall.push(obj);
        }
    }
    function ballMove() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        for(var i = 0; i < setBall.length; i++){
            var ball = setBall[i];
            ball.draw();
            ball.move();
            ball.links();
        }
        requestAnimationFrame(ballMove);
    }
    window.onload = function () {
        init();
        requestAnimationFrame(ballMove);
    }

    var animationEnd = (function () {
        var explorer = navigator.userAgent.toLowerCase();
        if(explorer.indexOf('chrome') != -1){
            return 'webkitTransitionEnd';
        }
        else if(explorer.indexOf("firefox") != -1){
            return 'transitionend';
        }
    })();
    var snsVisible = false;
    $(".js-toggle-sns-btn").click(function () {
        if(!snsVisible){
            $('.sns-btn').addClass('sns-visible');
            $('.sns-btn').css("visibility", "visible");
            snsVisible = true;
        }
        else{
            $('.sns-btn').removeClass('sns-visible').on(animationEnd, function () {
                $('.sns-btn').css("visibility", "hidden");
                snsVisible = false;
                $(this).off();
            })
        }
    })

    $('.nav-slider a:eq(0)').click(function () {
        $('.introEdit').css('display', 'none');
        $('.passwordChange').css('display', 'none');
        $('.profileEdit').css('display', 'block');
    })
    $('.nav-slider a:eq(1)').click(function () {
        $('.introEdit').css('display', 'block');
        $('.passwordChange').css('display', 'none');
        $('.profileEdit').css('display', 'none');
    })
    $('.nav-slider a:eq(2)').click(function () {
        $('.introEdit').css('display', 'none');
        $('.passwordChange').css('display', 'block');
        $('.profileEdit').css('display', 'none');
    })
	//ajax js start
  	function submitAjax(button, url, getJson) {
    $(button).click(function () {
        $.ajax({
            type: "POST",
            url: url,
            dataType: "json",
            data: getJson(),
            success: function (data) {
              if(data.ret==1 &&data.url!==undefined)
              	window.location.href =data.url;
                	
                win.alert('', data.msg);
            },
            error: function (jqXHR) {
                $("#msg").html(data.msg + "     出现了一些错误。");
            }
        })
    })
};
	function infoJson() {
		return {
			"name": $("#full_name").val(),
			"location": $("#location").val(),
			"job": $("#job").val(),
			"education": $("#education").val()
		};
	};

	function descJson() {
		return {
			"personalDesc": $("#personalDesc").val()
		};
	};

	function passwdJson() {
		return {
			"oldPasswd": $("#oldPasswd").val(),
			"newPasswd": $("#newPasswd").val(),
			"repeatPasswd": $("#repeatPasswd").val()
		};
	};
	$(document).ready(function () {
		submitAjax("#info", "/editinfo", infoJson);
		submitAjax("#description", "/editdesc",  descJson);
		submitAjax("#reset_passwd", "/resetpasswd", passwdJson);
	});
	//ajax js end
<?php echo '</script'; ?>
>
</html><?php }
}
