<?php
/* Smarty version 3.1.31, created on 2018-01-14 04:08:26
  from "/www/wwwroot/digital-store.tk/resources/views/zhihu/people.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a5a673a56e472_54156782',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c1423c4e7e3e2bb15fc6cfb8149139e4c3eacd11' => 
    array (
      0 => '/www/wwwroot/digital-store.tk/resources/views/zhihu/people.tpl',
      1 => 1515874102,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5a5a673a56e472_54156782 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<div class="zg-wrap zu-main clearFix" role="main">
        <div class="zu-main-content">
            <div class="zu-main-content-inner">
                <div class="zm-profile-header ProfileCard">
                    <div class="zm-profile-header-main">
                        <img class="Avatar Avatar--l" src="<?php echo $_smarty_tpl->tpl_vars['now_user']->value->gravatar;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['now_user']->value->full_name;?>
">
                        <div class="top">
                            <div class="title-section">
                                <span class="name"><?php echo $_smarty_tpl->tpl_vars['now_user']->value->full_name;?>
</span>
                            </div>
                        </div>
                        <div class="zm-profile-header-info">
                            <div class="zm-profile-header-user-describe">
                                <div class="items">
                                    <div class="item editable-group">
                                        <i class="icon icon-profile-location"></i>
                                        <span class="info-wrap">
                                            <span class="location item">
                                                <?php echo $_smarty_tpl->tpl_vars['now_user']->value->location;?>

                                            </span>
                                        </span>
                                    </div>
                                    <div class="item editable-group">
                                        <i class="icon icon-profile-company"></i>
                                        <span class="info-wrap">
                                            <span class="employment item">
                                                <?php echo $_smarty_tpl->tpl_vars['now_user']->value->job;?>

                                            </span>
                                        </span>
                                    </div>
                                    <div class="item editable-group">
                                        <i class="icon icon-profile-education"></i>
                                        <span class="info-wrap">
                                            <span class="education item">
                                                <?php echo $_smarty_tpl->tpl_vars['now_user']->value->education;?>

                                            </span>
                                        </span>
                                    </div>
                                </div>
                                <div class="zm-profile-header-describe editable-group">
                                    <span class="info-wrap fold-wrap fold">
                                        <span class="description unfold-itme">
                                            <span class="content">
												<?php echo $_smarty_tpl->tpl_vars['now_user']->value->description;?>

                                            </span>

                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="zm-profile-header-operation clearFix">
                        <div class="zm-profile-header-info-list">
                            <span class="zm-profile-header-info-title">统计</span>
                            <span class="zm-profile-header-user-agree">
                                <strong><?php echo $_smarty_tpl->tpl_vars['now_user']->value->agree_count;?>
</strong>赞同
                            </span>
                            <span class="zm-profile-header-user-thanks">
                                <strong><?php echo $_smarty_tpl->tpl_vars['now_user']->value->ask_count;?>
</strong>提问
                            </span>
							<span class="zm-profile-header-user-thanks">
                                <strong><?php echo $_smarty_tpl->tpl_vars['now_user']->value->answer_count;?>
</strong>回答
                            </span>
							<span class="zm-profile-header-user-thanks">
                                <strong><?php echo $_smarty_tpl->tpl_vars['now_user']->value->reply_count;?>
</strong>评论
                            </span>
                        </div>
						<?php if (!$_smarty_tpl->tpl_vars['is_me']->value) {?>
                        <div class="zm-profile-header-op-btn">
                          	
                            <?php if ($_smarty_tpl->tpl_vars['follow']->value) {?><button class="zg-btn zg-btn-unfollow zm-rich-follow-btn" onClick="follow(<?php echo $_smarty_tpl->tpl_vars['now_user']->value->id;?>
)" id="follow" >取消关注</button>
                          		  <?php } else { ?><button class="zg-btn zg-btn-follow zm-rich-follow-btn" onClick="follow(<?php echo $_smarty_tpl->tpl_vars['now_user']->value->id;?>
)" id="follow" style="background: #0e7bef;color:white;">关注</button><?php }?>
                        </div>
						<?php }?>
                    </div>
                </div>
            </div>
        </div>
        <div class="zu-main-sidebar">
            <div class="zm-profile-side-following clearFix">
                <a class="item" href="/people/<?php echo $_smarty_tpl->tpl_vars['now_user']->value->id;?>
/followees">
                    <span class="zg-gray-normal">关注了</span>
                    <br>
                    <strong><?php echo $_smarty_tpl->tpl_vars['count_followee']->value;?>
</strong>
                    <label>人</label>
                </a>
				<br>
                <br>
                <br>
                <a class="item" href="/people/<?php echo $_smarty_tpl->tpl_vars['now_user']->value->id;?>
/followers">
                    <span class="zg-gray-normal">关注者</span>
                    <br>
                    <strong><?php echo $_smarty_tpl->tpl_vars['count_follower']->value;?>
</strong>
                    <label>人</label>
                </a>
            </div>
    <?php echo '<script'; ?>
>

function follow(id){
		$.ajax({
			type:"POST",
			url:"/followpeople",
			dataType:"json",
			data:{
				id: id
			},
			success:function(data){
				if(data.ret){
					win.alert('', data.msg);
				}else{
					win.alert('', data.msg); 	
				}
			},
			error:function(jqXHR){
				win.alert('', data.msg);
			}
		});
}
<?php echo '</script'; ?>
>   
          <?php $_smarty_tpl->_subTemplateRender('file:footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
