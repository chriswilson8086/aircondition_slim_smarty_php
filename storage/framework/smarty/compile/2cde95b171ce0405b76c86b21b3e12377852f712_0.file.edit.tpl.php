<?php
/* Smarty version 3.1.31, created on 2018-01-14 01:39:10
  from "/www/wwwroot/digital-store.tk/resources/views/zhihu/admin/user/edit.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a5a443ec917f1_36850097',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '2cde95b171ce0405b76c86b21b3e12377852f712' => 
    array (
      0 => '/www/wwwroot/digital-store.tk/resources/views/zhihu/admin/user/edit.tpl',
      1 => 1515862371,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:admin/main.tpl' => 1,
    'file:dialog.tpl' => 1,
    'file:admin/footer.tpl' => 1,
  ),
),false)) {
function content_5a5a443ec917f1_36850097 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:admin/main.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>








	<main class="content">
		<div class="content-header ui-content-header">
			<div class="container">
				<h1 class="content-heading">用户编辑 #<?php echo $_smarty_tpl->tpl_vars['edit_user']->value->id;?>
</h1>
			</div>
		</div>
		<div class="container">
			<div class="col-lg-12 col-sm-12">
				<section class="content-inner margin-top-no">

					<div class="card">
						<div class="card-main">
							<div class="card-inner">
								<div class="form-group form-group-label">
									<label class="floating-label" for="email">邮箱</label>
									<input class="form-control" id="email" type="email" value="<?php echo $_smarty_tpl->tpl_vars['edit_user']->value->email;?>
">
								</div>
								

								<div class="form-group form-group-label">
									<label class="floating-label" for="passwd">密码(不修改请留空)</label>
									<input class="form-control" id="passwd" type="password">
								</div>

								<div class="form-group form-group-label">
									<div class="checkbox switch">
										<label for="is_admin">
											<input <?php if ($_smarty_tpl->tpl_vars['edit_user']->value->is_admin == 1) {?>checked<?php }?> class="access-hide" id="is_admin" type="checkbox"><span class="switch-toggle"></span>是否管理员
										</label>
									</div>
								</div>

								<div class="form-group form-group-label">
									<div class="checkbox switch">
										<label for="is_block">
											<input <?php if ($_smarty_tpl->tpl_vars['edit_user']->value->is_block == 1) {?>checked<?php }?> class="access-hide" id="is_block" type="checkbox"><span class="switch-toggle"></span>用户禁用
										</label>
									</div>
								</div>


							</div>
						</div>
					</div>


					<div class="card">
						<div class="card-main">
							<div class="card-inner">

								<div class="form-group">
									<div class="row">
										<div class="col-md-10 col-md-push-1">
											<button id="submit" type="submit" class="btn btn-block btn-brand waves-attach waves-light">修改</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<?php $_smarty_tpl->_subTemplateRender('file:dialog.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

			</div>



		</div>
	</main>











<?php $_smarty_tpl->_subTemplateRender('file:admin/footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>



<?php echo '<script'; ?>
>
	//document.getElementById("class_expire").value="<?php echo $_smarty_tpl->tpl_vars['edit_user']->value->class_expire;?>
";
    $(document).ready(function () {
        function submit() {
			if(document.getElementById('is_admin').checked)
			{
				var is_admin=1;
			}
			else
			{
				var is_admin=0;
			}

			if(document.getElementById('is_block').checked)
			{
				var is_block=1;
			}
			else
			{
				var is_block=0;
			}

            $.ajax({
                type: "PUT",
                url: "/admin/user/<?php echo $_smarty_tpl->tpl_vars['edit_user']->value->id;?>
",
                dataType: "json",
                data: {
                    email: $("#email").val(),
                    passwd: $("#passwd").val(),
                    is_block: is_block,
                    is_admin: is_admin
                },
                success: function (data) {
                    if (data.ret) {
                        $("#result").modal();
                        $("#msg").html(data.msg);
                        window.setTimeout("location.href=top.document.referrer", <?php echo $_smarty_tpl->tpl_vars['config']->value['jump_delay'];?>
);
                    } else {
                        $("#result").modal();
                        $("#msg").html(data.msg);
                    }
                },
                error: function (jqXHR) {
					$("#result").modal();
                    $("#msg").html(data.msg+"  发生了错误。");
                }
            });
        }

        $("html").keydown(function (event) {
            if (event.keyCode == 13) {
                login();
            }
        });
        $("#submit").click(function () {
            submit();
        });

    })
<?php echo '</script'; ?>
>
<?php }
}
