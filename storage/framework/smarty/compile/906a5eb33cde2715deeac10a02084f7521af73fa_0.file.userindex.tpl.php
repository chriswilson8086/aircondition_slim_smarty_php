<?php
/* Smarty version 3.1.31, created on 2018-05-27 10:10:14
  from "/www/wwwroot/panel.buptband.com/resources/views/buptband/userindex.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5b0a138680f031_06855747',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '906a5eb33cde2715deeac10a02084f7521af73fa' => 
    array (
      0 => '/www/wwwroot/panel.buptband.com/resources/views/buptband/userindex.tpl',
      1 => 1527386990,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:userheader.tpl' => 1,
    'file:dialog.tpl' => 1,
    'file:userfooter.tpl' => 1,
  ),
),false)) {
function content_5b0a138680f031_06855747 (Smarty_Internal_Template $_smarty_tpl) {
?>
<style type="text/css">
	body, html,.map {
      width: 100%;height: 300px;overflow: hidden;margin:0;font-family:"微软雅黑";
  }
  
	</style>
<?php $_smarty_tpl->_subTemplateRender('file:userheader.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>




	<main class="content">
		
		<div class="container">
			<section class="content-inner margin-top-no">
				<div class="ui-card-wrap">
						<div class="col-lg-12 col-md-12">
							<div class="card">
								<div class="card-main">
									<div class="card-inner margin-bottom-no">
                                        <nav class="tab-nav margin-top-no">
											<ul class="nav nav-list">
                                               <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['shows']->value, 'show');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['show']->value) {
?>
												<li <?php if ($_smarty_tpl->tpl_vars['show']->value['count'] == 0) {?>class="active"<?php }?>>
													<a class="waves-attach" data-toggle="tab" href="#<?php echo $_smarty_tpl->tpl_vars['show']->value['token'];?>
"><i class="icon icon-lg">person</i>&nbsp;<?php echo $_smarty_tpl->tpl_vars['show']->value['name'];?>
</a>
												</li>
                                                <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

											</ul>
										</nav>
                                      
										<div class="card-inner">
											<div class="tab-content">
                                              <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['shows']->value, 'show');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['show']->value) {
?>
                                              <div class="tab-pane fade <?php if ($_smarty_tpl->tpl_vars['show']->value['count'] == 0) {?>active in<?php }?>" id="<?php echo $_smarty_tpl->tpl_vars['show']->value['token'];?>
">
													<nav class="tab-nav margin-top-no">
														<ul class="nav nav-list">
															<li class="active">
																<a class="waves-attach" data-toggle="tab" href="#<?php echo $_smarty_tpl->tpl_vars['show']->value['token'];?>
1day"><i class="icon icon-lg">info_outline</i>&nbsp;当天记录</a>
															</li>
															<li>
																<a class="waves-attach" data-toggle="tab" href="#<?php echo $_smarty_tpl->tpl_vars['show']->value['token'];?>
3day"><i class="icon icon-lg">info_outline</i>&nbsp;前3天记录</a>
															</li>
															<li>
																<a class="waves-attach" data-toggle="tab" href="#<?php echo $_smarty_tpl->tpl_vars['show']->value['token'];?>
7day"><i class="icon icon-lg">info_outline</i>&nbsp;前7天记录</a>
															</li>
														</ul>
													</nav>
													<div class="tab-pane fade active in" id="<?php echo $_smarty_tpl->tpl_vars['show']->value['token'];?>
1day">
                                                        <h4><?php echo $_smarty_tpl->tpl_vars['show']->value['name'];?>
</h4>
                                                        <?php if ($_smarty_tpl->tpl_vars['show']->value['1day'] == 0) {?>
                                                        <p>暂无数据</p>
                                                        <?php } else { ?>
                                                      <div id="<?php echo $_smarty_tpl->tpl_vars['show']->value['token'];?>
_1map" class="map"></div>
														<!--调用地图1-->
                                                        <?php }?>
                                                        <p><a class="btn btn-brand btn-flat waves-attach" onclick="getnew_interface(<?php echo $_smarty_tpl->tpl_vars['show']->value['band_id'];?>
)"><span class="icon">info_outline</span>&nbsp;获取当前位置</a></p>
													</div>
													<div class="tab-pane fade" id="<?php echo $_smarty_tpl->tpl_vars['show']->value['token'];?>
3day">
														<h4><?php echo $_smarty_tpl->tpl_vars['show']->value['name'];?>
</h4>
														<?php if ($_smarty_tpl->tpl_vars['show']->value['3day'] == 0) {?>
                                                     
                                                        <p>暂无数据</p>
                                                        <?php } else { ?>
                                                      	<div id="<?php echo $_smarty_tpl->tpl_vars['show']->value['token'];?>
_3map" class="map"></div>
														<!--调用地图2-->
                                                        <?php }?>
                                                        <p><a class="btn btn-brand btn-flat waves-attach" onclick="getnew_interface(<?php echo $_smarty_tpl->tpl_vars['show']->value['band_id'];?>
)"><span class="icon">info_outline</span>&nbsp;获取当前位置</a></p>
													</div>
													<div class="tab-pane fade" id="<?php echo $_smarty_tpl->tpl_vars['show']->value['token'];?>
7day">
														<h4><?php echo $_smarty_tpl->tpl_vars['show']->value['name'];?>
</h4>
														<?php if ($_smarty_tpl->tpl_vars['show']->value['7day'] == 0) {?>
                                                        <p>暂无数据</p>
                                                        <?php } else { ?>
														<div id="<?php echo $_smarty_tpl->tpl_vars['show']->value['token'];?>
_5map" class="map"></div>
                                                        <?php }?>
                                                        <p><a class="btn btn-brand btn-flat waves-attach" onclick="getnew_interface(<?php echo $_smarty_tpl->tpl_vars['show']->value['band_id'];?>
)"><span class="icon">info_outline</span>&nbsp;获取当前位置</a></p>
													</div>
                                                </div>
                                              						<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

											</div>
										</div>
									</div>

								</div>
							</div>

						</div>
                  </div>


						<?php $_smarty_tpl->_subTemplateRender('file:dialog.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


					</div>


				</div>
			</section>
		</div>
	</main>







<?php $_smarty_tpl->_subTemplateRender('file:userfooter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>


<?php echo '<script'; ?>
>
function getnew_interface(id) {
	getnewid=id;
	getnew();
}
	function getnew(){
		$.ajax({
			type:"POST",
			url:"/user/getnew",
			dataType:"json",
			data:{
				id: getnewid
			},
			success:function(data){
				if(data.ret){
					$("#result").modal();
					$("#msg").html(data.msg);
                    setTimeout(function(){
                      window.location.reload();
                    },3000);
				}else{
					$("#result").modal();
					$("#msg").html(data.msg);
				}
			},
			error:function(jqXHR){
				$("#result").modal();
				$("#msg").html(data.msg+"  发生错误了。");
			}
		});
	}
<?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript" src="https://api.map.baidu.com/api?v=2.0&ak=kNHa5clW2KfILmGfjO9qPCcpnGZtOL5w"><?php echo '</script'; ?>
>

<?php echo '<script'; ?>
 type="text/javascript">
	//var points = [[116.362001, 39.96832], [116.3621, 39.967512], [116.362324, 39.96606], [116.364265, 39.965866], [116.365864, 39.966074], [116.366528, 39.967249], [116.364732, 39.967947], [116.365998, 39.968977], [116.367732, 39.96888], [116.366205, 39.96953], [116.365235, 39.970373]];

  var opts = {
    width: 250,// 信息窗口宽度
    height: 80,// 信息窗口高度
    title: "",// 信息窗口标题
    enableMessage: true //设置允许信息窗发送短息
};
var idArray = $(".map");
  for(var id_i=0;id_i<idArray.length;++id_i){
    var id =idArray[id_i].id;
var map = new BMap.Map(id);
<?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['shows']->value, 'show');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['show']->value) {
?>
if(id.indexOf("<?php echo $_smarty_tpl->tpl_vars['show']->value['token'];?>
")>-1){    
if (id.substr(id.length-4,1)=='1'){
    var points = [ <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['show']->value['1day'], 'locate');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['locate']->value) {
?> [  <?php echo $_smarty_tpl->tpl_vars['locate']->value['lat'];?>
  ,  <?php echo $_smarty_tpl->tpl_vars['locate']->value['lng'];?>
  , "<?php echo $_smarty_tpl->tpl_vars['locate']->value['time'];?>
"], <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>
]
}
else if (id.substr(id.length-4,1)=='3'){
    var points = [ <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['show']->value['3day'], 'locate');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['locate']->value) {
?> [  <?php echo $_smarty_tpl->tpl_vars['locate']->value['lat'];?>
  ,  <?php echo $_smarty_tpl->tpl_vars['locate']->value['lng'];?>
  , "<?php echo $_smarty_tpl->tpl_vars['locate']->value['time'];?>
"], <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>
]
}
else if (id.substr(id.length-4,1)=='7'){
    var points = [ <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['show']->value['7day'], 'locate');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['locate']->value) {
?> [  <?php echo $_smarty_tpl->tpl_vars['locate']->value['lat'];?>
  ,  <?php echo $_smarty_tpl->tpl_vars['locate']->value['lng'];?>
  , "<?php echo $_smarty_tpl->tpl_vars['locate']->value['time'];?>
"], <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>
]
}
}
<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

var point = new BMap.Point(points[0][0], points[0][1]);
map.centerAndZoom(point, 18);
map.enableScrollWheelZoom(true); //开启鼠标滚轮缩放
var pointList = [];
for (var i = 0; i < points.length; i++) {
    var point = new BMap.Point(points[i][0], points[i][1]);
    var marker = new BMap.Marker(point);
    map.addOverlay(marker);
    if (i == 0) { //以跳跃图标显示当前所在点坐标
        marker.setAnimation(BMAP_ANIMATION_BOUNCE)
    }
  
    var content = points[i][2];//信息窗口内容
    marker.addEventListener("click",function(e) {
       	var p = e.target;
      
    	var point = new BMap.Point(p.getPosition().lng, p.getPosition().lat);
    	var infoWindow = new BMap.InfoWindow(content, opts); // 创建信息窗口对象 
     // alert(content);
      $("#result").modal();
					$("#msg").html(content);
    	map.openInfoWindow(infoWindow, point); //开启信息窗口
    });
    pointList.push(point);
}
var polyline = new BMap.Polyline(pointList, { //添加折线
    strokeColor: "blue",
    strokeWeight: 2,
    strokeOpacity: 0.5
});
map.addOverlay(polyline);
  }


<?php echo '</script'; ?>
><?php }
}
