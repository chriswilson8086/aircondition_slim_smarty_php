<?php
/* Smarty version 3.1.31, created on 2018-04-01 18:16:19
  from "/www/wwwroot/panel.buptband.com/resources/views/buptband/addband.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5ac0b173050636_50632257',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3562bbbdaff6d05f73a11555c753f2d4b66b3306' => 
    array (
      0 => '/www/wwwroot/panel.buptband.com/resources/views/buptband/addband.tpl',
      1 => 1522577740,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:userheader.tpl' => 1,
    'file:dialog.tpl' => 1,
    'file:userfooter.tpl' => 1,
  ),
),false)) {
function content_5ac0b173050636_50632257 (Smarty_Internal_Template $_smarty_tpl) {
?>



<?php $_smarty_tpl->_subTemplateRender('file:userheader.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>








	<main class="content">
		<div class="content-header ui-content-header">
			<div class="container">
				<h1 class="content-heading"> 添加手环</h1>
			</div>
		</div>
		<div class="container">
			<div class="col-lg-12 col-sm-12">
				<section class="content-inner margin-top-no">
					<form id="main_form">
						<div class="card">
							<div class="card-main">
								<div class="card-inner">
									<div class="form-group form-group-label">
										<label class="floating-label" for="name">手环使用者姓名</label>
										<input class="form-control" id="name" name="name" type="text">
									</div>
									
									
									<div class="form-group form-group-label">
										<label class="floating-label" for="token">手环Token</label>
										<input class="form-control" id="token" name="token" type="text">
									</div>
																	
									
								</div>
                              
							</div>
						</div>
                      
                        <div class="card">
							<div class="card-main">
								<div class="card-inner">
                                    <div class="form-group form-group-label">
										<p>手环休眠起始时间和终止时间<code>手环将会在您设定的休眠时间段内进入休眠模式，请正确填写时间</code></p>
                                        <p><code>假如您需要手环在晚8点30分到次日8点整休眠，请填写起始小时为20，起始分钟为30，结束小时为8，结束分钟为0</code></p>
                                        <p><code>假如您不需要手环休眠，则全部填写为0即可</code></p>
									</div>
                                  
									<div class="form-group form-group-label">
										<label class="floating-label" for="start_hour">起始小时</label>
										<input class="form-control" id="start_hour" name="start_hour" type="text">
									</div>
									
									
									<div class="form-group form-group-label">
										<label class="floating-label" for="start_min">起始分钟</label>
										<input class="form-control" id="start_min" name="start_min" type="text">
									</div>
                                  
                                    <div class="form-group form-group-label">
										<label class="floating-label" for="end_hour">结束小时</label>
										<input class="form-control" id="end_hour" name="end_hour" type="text">
									</div>
                                  
                                    <div class="form-group form-group-label">
										<label class="floating-label" for="end_min">结束分钟</label>
										<input class="form-control" id="end_min" name="end_min" type="text">
									</div>
									
									
								</div>
                              
							</div>
						</div>
						
						
						<div class="card">
							<div class="card-main">
								<div class="card-inner">
									
									<div class="form-group">
										<div class="row">
											<div class="col-md-10 col-md-push-1">
												<button id="submit" type="submit" class="btn btn-block btn-brand waves-attach waves-light">添加</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>	
			<?php $_smarty_tpl->_subTemplateRender('file:dialog.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

			</div>
			
			
		</div>
	</main>

	
	
	
	






<?php $_smarty_tpl->_subTemplateRender('file:userfooter.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>



<?php echo '<script'; ?>
>

	$('#main_form').validate({
      
    submitHandler: function() {
			
			
			
		$.ajax({

				type: "POST",
				url: "/user/band",
				dataType: "json",
				data: {
					    name: $("#name").val(),
					    token: $("#token").val(),
					    start_hour: $("#start_hour").val(),
					    start_min: $("#start_min").val(),
                  		end_hour: $("#end_hour").val(),
                  		end_min: $("#end_min").val(),
					},
					success: function (data) {
					    if (data.ret) {
						$("#result").modal();
						$("#msg").html(data.msg);
						window.setTimeout("location.href=top.document.referrer", 1000);
					    } else {
						$("#result").modal();
						$("#msg").html(data.msg);
					    }
					},
					error: function (jqXHR) {
					    $("#result").modal();
					    $("#msg").html(data.msg+"  发生错误了。");
					}
					});
				}
		});

<?php echo '</script'; ?>
>

<?php }
}
