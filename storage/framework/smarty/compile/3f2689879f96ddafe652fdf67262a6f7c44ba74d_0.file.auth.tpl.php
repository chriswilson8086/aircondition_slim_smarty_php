<?php
/* Smarty version 3.1.31, created on 2018-01-17 21:16:20
  from "/www/wwwroot/digital-store.tk/resources/views/zhihu/auth.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a5f4ca41a6609_83626344',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '3f2689879f96ddafe652fdf67262a6f7c44ba74d' => 
    array (
      0 => '/www/wwwroot/digital-store.tk/resources/views/zhihu/auth.tpl',
      1 => 1516174191,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a5f4ca41a6609_83626344 (Smarty_Internal_Template $_smarty_tpl) {
?>
<!DOCTYPE html>
<html lang="zh-CN" style="height: 100%; display: block">
<link href="/theme/window/window.css" rel="stylesheet" />
<?php echo '<script'; ?>
 src="/theme/window/window.js"><?php echo '</script'; ?>
>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $_smarty_tpl->tpl_vars['config']->value["appName"];?>
</title>
    <style type="text/css">
        html{
            padding: 0;
            margin: 0 ;
            height: 100% !important;
        }
        body{
            margin: 0;
            height: 100%;
            color: #555;
            margin: 0;
            padding: 0;
            width: 100%;
            display: flex;
            align-items: center;
            font-size: 14px;
            background: #f7fafc;
        }
        input, button{
            outline: none;
        }
        a{
            text-decoration: none;
        }
        .logo{
            margin: 0 auto;
            width: 160px;
            height: 74px;
            background: url("/theme/zhihu/img/logo.png") no-repeat;
            text-align: center;
            background-size: contain;
        }
        .subtitle{
            margin: 30px 0 20px;
            font-weight: 400;
            font-size: 18px;
            line-height: 1;
            text-align: center;
        }
        .index-main{
            width: 100%;
            height: 100%;
            //margin-bottom: 42px;
            text-align: center;
        }
        .index-main:before{
            content: "";
            display: inline-block;
            height: 100%;
            vertical-align: middle;
        }
        .index-main-body{
            padding: 0;
            display: inline-block;
            vertical-align: middle;
            width: 300px;
            min-height: 590px;
            margin: auto;
        }
        .index-header{
            text-align: center;
        }
        .index-main-body p{
            width: inherit;
            text-align: center;
            opacity: 0.5;
        }
        .sign-nav{
            position: relative;
            text-align: center;
            font-size: 18px;
            color: black;
            margin-bottom: 10px;
        }
        .sign-nav a{
            float: left;
            display: inline-block;
            width: 4em;
            line-height: 35px;
            text-decoration: none;
            opacity: 0.6;
            transition: opacity 0.5s;
            color: inherit;
        }
        .sign-nav a:hover{
            opacity: 1;
        }
        .sign-nav a.active{
            opacity: 1;
            color: #0f88eb;
        }
        .nav-slider-bar{
            position: absolute;
            margin: 0 0.8em;
            background: #0f88eb;
            height: 2px;
            width: 2.4em;
            left: 0;
            bottom: 0;
            transition: left .15s linear;
        }
        .sign-nav[data-index="1"] .nav-slider-bar{
            left: 4em;
        }
        .group-input{
            border: 1px solid #d5d5d5;
            border-radius: 3px;
        }
        .input-wrapper{
            margin: 0;
            position: relative;
        }
        div.input-wrapper input{
            width: 100%;
            padding: 1em 0.8em;
            line-height: 19px;
            box-sizing: border-box;
            color: #555;
            border: 0;
        }
        .input-wrapper + .input-wrapper{
            border-top: 1px solid #e8e8e8;
        }
        .button-wrapper{
            margin-top: 18px;
            display: block;
        }
        .login-btn{
            background: #0f88eb;
            line-height: 41px;
            color: white;
            border-radius: 3px;
            width: 100%;
            font-size: 15px;
            text-align: center;
            cursor: pointer;
            border: 0;
        }
        .email-btn{
            background: #ff4081;
            line-height: 41px;
            color: white;
            border-radius: 3px;
            width: 100%;
            font-size: 15px;
            text-align: center;
            cursor: pointer;
            border: 0;
        }
        .signin-misc-wrapper, .weibo-signup-wrapper{
            margin-top: 18px;
            display: block;
        }
        .signin-misc-wrapper .remember-me{
            float: left;
        }
        .signin-misc-wrapper .unable-login{
            float: right;
            color: inherit;
        }
        .clearFix:after, .clearFix:before{
            content: "";
            display: block;
            clear: both;
            visibility: hidden;
            line-height: 0;
            height: 0;
            font-size: 0;
        }
        .footer{
            position: fixed;
            width: 100%;
            left: 0;
            bottom: 0;
            font-size: 12px;
            line-height: 42px;
            text-align: center;
        }
        .footer a, .footer span{
            color: #aebdc9;
        }
        .dot{
            margin: 0 3px;
        }
    </style>
</head>
<body>
    <div class="index-main">
        <div class="index-main-body">
            <div class="index-header">
                <h1 class="logo"></h1>
                <h2 class="subtitle">与世界分享你的知识，经验和见解</h2>
            </div>
            <div class="sign-flow">
                <div class="sign-nav" data-index="0">
                    <div class="nav-slider" style="display: inline-block; position: relative">
                        <a href="#signup" class="active">注册</a>
                        <a href="#signin">登录</a>
                        <span class="nav-slider-bar"></span>
                    </div>
                </div>
                <div class="sign-login" style="display: none">
                    <form novalidate="novalidate">
                        <div class="group-input">
                            <div class="email input-wrapper">
                                <input type="text" id="login_email" name="login_email" placeholder="邮箱" required>
                            </div>
                            <div class="input-wrapper">
                                <input type="password" id="login_passwd" name="login_passwd" placeholder="密码" required>
                            </div>
                        </div>
                        <div class="button-wrapper">
                            <button class="login-btn" id="login" type="button">登录</button>
                        </div>
                        <div class="signin-misc-wrapper clearFix">
                            <label class="remember-me">
                                <input type="checkbox" id="remember-me" name="remember-me" checked>
                                记住我
                            </label>
                            <a href="/auth/forgetpasswd" class="unable-login">忘记密码</a>
                        </div>
                    </form>
                </div>
                <div class="sign-lognup" style="display: block">
                    <form novalidate="novalidate">
                        <div class="group-input">
                            <div class="name input-wrapper">
                                <input type="text" name="name" id="reg_name" placeholder="姓名" required>
                            </div>
                            <div class="email input-wrapper">
                                <input type="text" id="reg_email" name="reg_email" placeholder="邮箱" required>
                            </div>
                            <div class="input-wrapper">
                                <input type="password" id="reg_passwd" name="reg_passwd" placeholder="密码" required>
                            </div>
                            <div class="input-wrapper captcha-module">
                                <input id="email_code" name="email_code" placeholder="邮箱验证码" required>
                            </div>
                        </div>
                        <div class="button-wrapper">
                            <button class="email-btn" id="email_verify"  type="button">获取邮箱验证码</button>
                        </div>
                        <div class="button-wrapper">
                            <button class="login-btn" id="reg"  type="button">注册假的知乎</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <span>@假的知乎</span>
        <span class="dot">·</span>
        <a target="_blank" href="#">打死不备案</a>
    </div>
    <canvas id="canvas" style="position: absolute; top: 0; left: 0; z-index: -1"></canvas>
</body>
<?php echo '<script'; ?>
 src="//cdn.staticfile.org/jquery/2.2.1/jquery.min.js"><?php echo '</script'; ?>
>
<?php echo '<script'; ?>
 type="text/javascript">
    function stopBubble(e) {
        if(e && e.stopPropagation()){
            e.stopPropagation();
        }
        else{
            window.event.cancelBubble = true;
        }
    }
    var canvas = document.getElementById("canvas");
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    var ctx = canvas.getContext('2d');
    function rand(min, max) {
        return parseInt(Math.random()*(max - min) + min);
    }
    function Round() {
        /*半径*/
        this.r = rand(5, 15);
        var speed = rand(1, 3);
        this.speedX = rand(0, 4) > 2 ? speed: -speed;
        this.speedY = rand(0, 4) > 2 ? speed: -speed;
        var x = rand(this.r, canvas.width - this.r);
        this.x = x < this.r ? this.r : x;
        var y = rand(this.r, canvas.height - this.r);
        this.y = y < this.y ? this.r : y;
    }
    Round.prototype.draw = function () {
        ctx.fillStyle = 'rgba(200, 200, 200, 0.2)';
        ctx.beginPath();
        ctx.arc(this.x, this.y, this.r, 0, 2* Math.PI, true);
        ctx.closePath();
        ctx.fill();
    }
    Round.prototype.move = function () {
        this.x += this.speedX/10;
        if(this.x > canvas.width || this.x < 0){
            this.speedX *= -1;
        }
        this.y += this.speedY/10;
        if(this.y > canvas.height || this.y < 0){
            this.speedY *= -1;
        }
    }
    Round.prototype.links = function () {
        for(var loop = 0; loop < setBall.length; loop++){
            var len = Math.sqrt(((this.x - setBall[loop].x)*(this.x - setBall[loop].x)) + ((this.y-setBall[loop].y)*(this.y-setBall[loop].y)));
            var line = 1/len * 2;
            if(len < 250){
                ctx.beginPath();
                ctx.strokeStyle = 'rgba(0, 0, 0, '+ line + ')';
                ctx.moveTo(this.x, this.y);
                ctx.lineTo(setBall[loop].x, setBall[loop].y);
                ctx.stroke();
                ctx.closePath();
            }
        }
    }
    var setBall = [];
    function init() {
        for(var num = 0; num < 50; num++){
            var obj = new Round();
            obj.draw();
            obj.move();
            setBall.push(obj);
        }
    }
    function ballMove() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        for(var i = 0; i < setBall.length; i++){
            var ball = setBall[i];
            ball.draw();
            ball.move();
            ball.links();
        }
        requestAnimationFrame(ballMove);
    }
    window.onload = function () {
        init();
        requestAnimationFrame(ballMove);
    }

    var animationEnd = (function () {
        var explorer = navigator.userAgent.toLowerCase();
        if(explorer.indexOf('chrome') != -1){
            return 'webkitTransitionEnd';
        }
        else if(explorer.indexOf("firefox") != -1){
            return 'transitionend';
        }
    })();
    var navSlideIndex = 0;
    $('.nav-slider a:first').click(function () {
        if(navSlideIndex == 1){
            $(this).addClass('active');
            $('.sign-nav').attr("data-index", "0");
            $('.nav-slider a:last').removeClass('active');
            $('.sign-login').css('display', 'none');
            $('.sign-lognup').css('display', 'block');
            navSlideIndex = 0;
        }
    })
    $('.nav-slider a:last').click(function () {
        if(navSlideIndex == 0){
            $(this).addClass('active');
            $('.sign-nav').attr("data-index", "1");
            $('.nav-slider a:first').removeClass('active');
            $('.sign-login').css('display', 'block');
            $('.sign-lognup').css('display', 'none');
            navSlideIndex = 1;
        }
    })

<?php echo '</script'; ?>
>
 <?php echo '<script'; ?>
>
    $(document).ready(function(){
        function register(){
            $.ajax({
                type:"POST",
                url:"/auth/register",
                dataType:"json",
                data:{
                    name: $("#reg_name").val(),
                    email: $("#reg_email").val(),
                    passwd: $("#reg_passwd").val(),
                    email_code: $("#email_code").val()
                },
                success:function(data){
                    if(data.ret == 1){
                        win.alert('', data.msg); 
                        window.setTimeout("location.href='/auth'", 1000);
                    }else{
                        win.alert('', data.msg); 
                    }
                },
                error:function(jqXHR){
			$("#msg-error-p").html("发生错误："+jqXHR.status);
                }
            });
        }
		$("#reg").click(function(){
            register();
        });
		
    })
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
    $(document).ready(function(){
        function login(){
            $.ajax({
                type:"POST",
                url:"/auth/login",
                dataType:"json",
                data:{
                    email: $("#login_email").val(),
                    passwd: $("#login_passwd").val(),
                    remember_me: $("#remember_me:checked").val()
                },
                success:function(data){
                    if(data.ret == 1){
                        win.alert('', data.msg); 
                        window.setTimeout("location.href='/'", 1000);
                    }else{
                        win.alert('', data.msg); 
                    }
                },
                error:function(jqXHR){
			$("#msg-error-p").html("发生错误："+jqXHR.status);
                }
            });
        }
        $("#login").click(function(){
            login();
        });
    })
<?php echo '</script'; ?>
>
<?php echo '<script'; ?>
>
var wait=60;
function time(o) {
		if (wait == 0) {
			o.removeAttr("disabled");			
			o.text("获取验证码");
			wait = 60;
		} else {
			o.attr("disabled","disabled");
			o.text("重新发送(" + wait + ")");
			wait--;
			setTimeout(function() {
				time(o)
			},
			1000)
		}
	}



    $(document).ready(function () {
        $("#email_verify").click(function () {
			time($("#email_verify"));
			
            $.ajax({
                type: "POST",
                url: "/auth/send",
                dataType: "json",
                data: {
                    email: $("#reg_email").val()
                },
                success: function (data) {
                    if (data.ret) {
                      win.alert('', data.msg); 						
                    } else {
                       win.alert('', data.msg); 
                    }
                },
                error: function (jqXHR) {
                    $("#result").modal();
			$("#msg").html(data.msg+"     出现了一些错误。");
                }
            })
        })
    })
<?php echo '</script'; ?>
>
</html><?php }
}
