<?php
/* Smarty version 3.1.31, created on 2018-01-14 04:52:58
  from "/www/wwwroot/digital-store.tk/resources/views/zhihu/follow.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a5a71aa773f25_48332560',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '17d8a6b0ccc0ca0e1bec439359c90716b7fb26c6' => 
    array (
      0 => '/www/wwwroot/digital-store.tk/resources/views/zhihu/follow.tpl',
      1 => 1515876773,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5a5a71aa773f25_48332560 (Smarty_Internal_Template $_smarty_tpl) {
$_smarty_tpl->_subTemplateRender('file:header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>

<div class="zg-wrap zu-main clearFix" role="main">
       <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['shows']->value, 'show');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['show']->value) {
?>
        <div class="zu-main-content">
            <div class="zu-main-content-inner">
                <div class="zm-profile-header ProfileCard">
                    <div class="zm-profile-header-main">
                        <img class="Avatar Avatar--l" src="<?php echo $_smarty_tpl->tpl_vars['show']->value['follow']->gravatar;?>
" alt="<?php echo $_smarty_tpl->tpl_vars['show']->value['follow']->full_name;?>
">
                        <div class="top">
                            <div class="title-section">
                                <span class="name"><?php echo $_smarty_tpl->tpl_vars['show']->value['follow']->full_name;?>
</span>
                            </div>
                        </div>
                        <div class="zm-profile-header-info">
                            <div class="zm-profile-header-user-describe">
                                <div class="items">
                                    <div class="item editable-group">
                                        <i class="icon icon-profile-location"></i>
                                        <span class="info-wrap">
                                            <span class="location item">
                                                <?php echo $_smarty_tpl->tpl_vars['show']->value['follow']->location;?>

                                            </span>
                                        </span>
                                    </div>
                                    <div class="item editable-group">
                                        <i class="icon icon-profile-company"></i>
                                        <span class="info-wrap">
                                            <span class="employment item">
                                                <?php echo $_smarty_tpl->tpl_vars['show']->value['follow']->job;?>

                                            </span>
                                        </span>
                                    </div>
                                    <div class="item editable-group">
                                        <i class="icon icon-profile-education"></i>
                                        <span class="info-wrap">
                                            <span class="education item">
                                                <?php echo $_smarty_tpl->tpl_vars['show']->value['follow']->education;?>

                                            </span>
                                        </span>
                                    </div>
                                </div>
                                <div class="zm-profile-header-describe editable-group">
                                    <span class="info-wrap fold-wrap fold">
                                        <span class="description unfold-itme">
                                            <span class="content">
												<?php echo $_smarty_tpl->tpl_vars['show']->value['follow']->description;?>

                                            </span>

                                        </span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="zm-profile-header-operation clearFix">
                        <div class="zm-profile-header-info-list">
                            <span class="zm-profile-header-info-title">统计</span>
                            <span class="zm-profile-header-user-agree">
                                <strong><?php echo $_smarty_tpl->tpl_vars['show']->value['follow']->agree_count;?>
</strong>赞同
                            </span>
                            <span class="zm-profile-header-user-thanks">
                                <strong><?php echo $_smarty_tpl->tpl_vars['show']->value['follow']->ask_count;?>
</strong>提问
                            </span>
							<span class="zm-profile-header-user-thanks">
                                <strong><?php echo $_smarty_tpl->tpl_vars['show']->value['follow']->answer_count;?>
</strong>回答
                            </span>
							<span class="zm-profile-header-user-thanks">
                                <strong><?php echo $_smarty_tpl->tpl_vars['show']->value['follow']->reply_count;?>
</strong>评论
                            </span>
                        </div>
						<?php if ($_smarty_tpl->tpl_vars['show']->value['type'] != 2) {?>
                        <div class="zm-profile-header-op-btn">
                          	<?php if ($_smarty_tpl->tpl_vars['show']->value['type'] == 1) {?><button class="zg-btn zg-btn-unfollow zm-rich-follow-btn" onClick="follow(<?php echo $_smarty_tpl->tpl_vars['show']->value['follow']->id;?>
)" id="follow">取消关注</button>
                            <?php } else { ?><button class="zg-btn zg-btn-unfollow zm-rich-follow-btn" onClick="follow(<?php echo $_smarty_tpl->tpl_vars['show']->value['follow']->id;?>
)" id="follow" style="background: #0e7bef;color:white;">关注</button><?php }?>
                        </div>
						<?php }?>
                    </div>
                </div>
            </div>
        </div>
		<?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

		<?php echo $_smarty_tpl->tpl_vars['follows']->value->render();?>

   <?php echo '<script'; ?>
>

function follow(id){
		$.ajax({
          	async:false,
			type:"POST",
			url:"/followpeople",
			dataType:"json",
			data:{
				id: id
			},
			success:function(data){
				if(data.ret){
					win.alert('', data.msg);
				}else{
					win.alert('', data.msg); 	
				}
			},
			error:function(jqXHR){
				win.alert('', data.msg);
			}
		});
  		location=location;
}
<?php echo '</script'; ?>
>   
		        <div class="zu-main-sidebar">
          <?php $_smarty_tpl->_subTemplateRender('file:footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
