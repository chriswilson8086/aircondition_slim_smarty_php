<?php
/* Smarty version 3.1.31, created on 2018-01-13 23:16:30
  from "/www/wwwroot/digital-store.tk/resources/views/zhihu/table/table.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5a5a22ce0f2a53_29125433',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '1ba1f45558d3b6bf70df48396cfc3a79ac590d64' => 
    array (
      0 => '/www/wwwroot/digital-store.tk/resources/views/zhihu/table/table.tpl',
      1 => 1514464534,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5a5a22ce0f2a53_29125433 (Smarty_Internal_Template $_smarty_tpl) {
?>
<table id="table_1" class="mdl-data-table" cellspacing="0" width="100%">
  <thead>
    <tr>
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['table_config']->value['total_column'], 'value', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
        <th class="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</th>
      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    </tr>
  </thead>
  <tfoot>
    <tr>
      <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['table_config']->value['total_column'], 'value', false, 'key');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['key']->value => $_smarty_tpl->tpl_vars['value']->value) {
?>
        <th class="<?php echo $_smarty_tpl->tpl_vars['key']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['value']->value;?>
</th>
      <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);
?>

    </tr>
  </tfoot>
  <tbody>
  </tbody>
</table>
<?php }
}
