<?php
/* Smarty version 3.1.31, created on 2018-03-29 16:58:18
  from "/www/wwwroot/panel.buptband.com/resources/views/buptband/405.tpl" */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.31',
  'unifunc' => 'content_5abcaaaaa14079_81818131',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'cd0410a54eee65871bb7f6745b0a6a8f85766701' => 
    array (
      0 => '/www/wwwroot/panel.buptband.com/resources/views/buptband/405.tpl',
      1 => 1522313121,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:header.tpl' => 1,
    'file:footer.tpl' => 1,
  ),
),false)) {
function content_5abcaaaaa14079_81818131 (Smarty_Internal_Template $_smarty_tpl) {
?>





<?php $_smarty_tpl->_subTemplateRender('file:header.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>



	<main class="content">
		<div class="content-header ui-content-header">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-lg-push-0 col-sm-12 col-sm-push-0">
						<h1 class="content-heading">405</h1>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
						<section class="content-inner margin-top-no">
							
							
							<div class="card">
								<div class="card-main">
									<div class="card-inner">
										<p>您的访问方式不正确。</p>
									</div>
									
									<div class="card-action">
										<div class="card-action-btn pull-left">
											<a class="btn btn-flat waves-attach" href="javascript:history.back()"><span class="icon">backspace</span>&nbsp;返回</a>
										</div>
									</div>
									
								</div>
							</div>

								
							<div class="card">
								<div class="card-main">
									<div class="card-inner">
											<div class="card-img">
												<img src="https://http.cat/405" style="width: 100%;">
											</div>
									</div>
									
									
								</div>
							</div>
							

		
							
						</section>
			
			
			
		</div>
	</main>


<?php $_smarty_tpl->_subTemplateRender('file:footer.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
}
}
